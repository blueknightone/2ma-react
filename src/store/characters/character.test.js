import actions from './actions';
import reducer from './reducer';
import selectors from './selectors';
import types from './types';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import database from '../../firebase/firebase';
import { characterOperations } from ".";

const characters = [
    {
        id: 'character-1',
        name: 'Jean-Luc Picard',
        imageURL: 'http://via.placeholder.com/200x300?text=Picard',
        levelId: 'off-capt',
        characterClassId: 'pos-commanding-officer',
        experience: 49000.66,
        strength: 5,
        dexterity: 5,
        constitution: 5,
        intelligence: 5,
        awareness: 5,
        luck: 5,
        gender: 'Male',
        race: 'Human',
        age: '67 Terran Years',
        height: '1.78 meters',
        weight: '82.0 kg',
        description: 'Jean-Luc Picard was a celebrated Starfleet officer, archaeologist and diplomat who served during the latter two thirds of the 24th century. He is an average-height, bald, terran male with a British accent. He enjoys Earl Grey tea (hot), detective holo-novels, and fencing. He has a habit of tugging his uniform jacket straight whenever he stands up from a chair.',
        background: "The highlights of his career were centered around assignments as commanding officer of the Federation starships: USS Stargazer, USS Enterprise-D, and the USS Enterprise-E. In these roles, Picard not only witnessed the major turning points of recent galactic history, but played a key role in them also, from making first contact as captain of the Federation's flagship with no fewer than 27 alien species, including the Ferengi and the Borg.",
        personality: 'Picard held diverse intellectual interests and recreational pursuits. He was a lifelong avocational archaeologist, inspired by his Academy instructor, Richard Galen,',
        inventory: ['Com-badge', 'PADD', 'Rissekan Flute'],
        player: 'OAA866fkjsgwhfOdbMRfzHaRfzG2',
        createdBy: 'OAA866fkjsgwhfOdbMRfzHaRfzG2',
        createdAt: 1544564646964,
        archived: true,
        approvedBy: 'user-1',
        groupId: 'group-1',
    },
    {
        id: 'character-2',
        name: 'William Riker',
        imageURL: 'http://via.placeholder.com/200x300',
        levelId: 'off-capt',
        characterClassId: 'pos-commanding-officer',
        experience: 12345.66,
        strength: 0,
        dexterity: 2,
        constitution: 3,
        intelligence: 4,
        awareness: 5,
        luck: 6,
        gender: 'Male',
        race: 'Human',
        age: '40 Terran Years',
        height: '1.9 meters',
        weight: '87.0 kg',
        description: 'Has a beard',
        background: "Has a beard. 'Nuff said.",
        personality: "Sometimes doesn't have a beard. We don't talk about those times.",
        inventory: ['Com-badge', 'PADD', 'Phaser'],
        player: '',
        createdBy: 'OAA866fkjsgwhfOdbMRfzHaRfzG2',
        createdAt: 1544564646964,
        archived: false,
        approvedBy: '',
        groupId: 'group-2',
    },
    {
        id: 'character-3',
        name: 'Data',
        imageURL: 'http://via.placeholder.com/200x300',
        levelId: 'off-cdr',
        characterClassId: 'pos-executive-officer',
        experience: 12345.66,
        strength: 5,
        dexterity: 5,
        constitution: 5,
        intelligence: 5,
        awareness: 5,
        luck: 5,
        gender: 'Male',
        race: 'Android',
        age: '40 Terran Years',
        height: '1.9 meters',
        weight: '87.0 kg',
        description: 'Gold skinned and super smart.',
        background: "Has a beard. 'Nuff said.",
        personality: "Was a bit too analytical before the emotion chip.",
        inventory: ['Com-badge', 'PADD', 'Phaser'],
        player: '',
        createdBy: 'OAA866fkjsgwhfOdbMRfzHaRfzG2',
        createdAt: 1544564646964,
        archived: false,
        approvedBy: '',
        groupId: 'group-1',
    },
    {
        id: 'character-4',
        name: 'Nog',
        imageURL: 'http://via.placeholder.com/200x300',
        levelId: 'off-ltcdr',
        characterClassId: 'pos-chief-sec-tac',
        experience: 12345.66,
        strength: 5,
        dexterity: 5,
        constitution: 5,
        intelligence: 5,
        awareness: 5,
        luck: 5,
        gender: 'Male',
        race: 'Ferengi',
        age: '25 Terran Years',
        height: '1.9 meters',
        weight: '87.0 kg',
        description: 'Sharp teeth, big ears, knobby skull',
        background: "First Ferengi in Starfleet",
        personality: "Always looking for that next, good business deal.",
        inventory: ['Com-badge', 'PADD', 'Phaser'],
        player: '',
        createdBy: 'OAA866fkjsgwhfOdbMRfzHaRfzG2',
        createdAt: 1544564646964,
        archived: false,
        approvedBy: '',
        groupId: '',
    }
];

describe('actions', () => {
    it('should setup addCharacter action object', () => {
        const action = actions.addCharacter(characters[0]);
        expect(action).toEqual({ type: types.ADD_CHARACTER, character: characters[0] });
    });

    it('should setup setCharacters action object', () => {
        const action = actions.setCharacters(characters);
        expect(action).toEqual({ type: types.SET_CHARACTERS, characters });
    });

    it('should setup editCharacter action object', () => {
        const id = 'abc123';
        const update = { name: 'Test' };
        const action = actions.editCharacter(id, update);
        expect(action).toEqual({ type: types.EDIT_CHARACTER, id, updates: update });
    });

    it('should setup removeCharacter action object', () => {
        const id = 'abc123';
        const action = actions.removeCharacter({ id });
        expect(action).toEqual({ type: types.REMOVE_CHARACTER, id });
    });
});

describe('reducers', () => {
    it('should add character to the store', () => {
        const action = actions.addCharacter(characters[0]);
        const state = reducer({ ...characters[0] }, action);
        expect(state).toEqual([{ ...characters[0] }]);
    });

    it('should remove character from, the store', () => {
        const action = actions.removeCharacter({ id: characters[0].id });
        const state = reducer(characters, action);
        expect(state).toEqual([characters[1], characters[2], characters[3]]);
    });
    it('should update a character in the store', () => {
        const updatedCharacter = {
            ...characters[1],
            name: 'Updated Name',
        };
        const action = actions.editCharacter(characters[1].id, { name: 'Updated Name' });
        const state = reducer(characters, action);
        expect(state).toEqual([characters[0], updatedCharacter, characters[2], characters[3]]);
    });
    it('should return all characters', function () {
        const action = actions.setCharacters(characters);
        const state = reducer({}, action);
        expect(state).toEqual(characters);
    });
    it('should return the state when no action matches', function () {
        const state = reducer('test-state', 'bad-action');
        expect(state).toEqual('test-state');
    });
});

describe('selectors', function () {
    it('should return alphabetically sorted characters', () => {
        const result = selectors.sortCharactersByName(characters);
        const expected = [
            characters[2],
            characters[0],
            characters[3],
            characters[1],
        ];
        expect(result).toEqual(expected);
    });

    it('should return characters with correct group id', function () {
        const expected = JSON.stringify([characters[0], characters[2]]);
        const result = JSON.stringify(selectors.findAllCharactersInGroup(characters, 'group-1'));
        expect(result).toEqual(expected);
    });
});

describe('operations', () => {
    const createMockStore = configureMockStore([thunk]);
    const uid = 'this-is-my-test-uid';
    const defaultAuthState = { auth: { uid } };

    beforeEach((done) => {
        const characterData = {};
        characters.forEach((character) => {
            characterData[character.id] = { ...character }
        });
        database.ref(`characters`).set(characterData).then(() => {
            done();
        });
    });

    it('should remove a character from firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        const id = characters[2].id;
        // noinspection JSUnresolvedFunction
        store.dispatch(characterOperations.startDeleteCharacter({ id }))
            .then(() => {
                // noinspection JSUnresolvedFunction
                const actions = store.getActions({ auth: { uid } });
                expect(actions[0]).toEqual({
                    type: types.REMOVE_CHARACTER,
                    id,
                });

                return database.ref(`characters/${id}`).once('value');
            })
            .then((snapshot) => {
                expect(snapshot.val()).toBeFalsy();
                done();
            });
    });

    it('should edit (update) a character in Firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        const id = characters[1].id;
        const updates = { name: 'Test Name Change' };
        // noinspection JSUnresolvedFunction
        store.dispatch(characterOperations.startUpdateCharacter(id, updates))
            .then(() => {
                // noinspection JSUnresolvedFunction
                const actions = store.getActions();
                expect(actions[0]).toEqual({
                    type: types.EDIT_CHARACTER,
                    id,
                    updates,
                });
                return database.ref(`characters/${id}`).once('value');
            })
            .then((snapshot) => {
                expect(snapshot.val().name).toEqual(updates.name);
                done();
            });
    });

    it('should add character to Firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        const characterData = {
            name: 'TestName',
            imageURL: 'path/to/image.jpg',
            levelId: 'level-1',
            characterClassId: 'class-1',
            experience: 0,
            strength: 1,
            dexterity: 2,
            constitution: 3,
            intelligence: 4,
            awareness: 5,
            luck: 6,
            gender: 'gender',
            race: 'race',
            age: 'age',
            height: 'height',
            weight: 'weight',
            description: 'desc',
            background: 'bg',
            personality: 'pers',
            inventory: ['item'],
            player: 'player-1',
            createdBy: 'player-2',
            archived: false,
            approvedBy: 'player-3',
            groupId: 'group-1',
        };
        // noinspection JSUnresolvedFunction
        store.dispatch(characterOperations.startAddCharacter(characterData))
            .then(() => {
                // noinspection JSUnresolvedFunction
                const actions = store.getActions();
                expect(actions[0]).toEqual({
                    type: types.ADD_CHARACTER,
                    character: {
                        id: expect.any(String),
                        ...characterData,
                    }
                });

                return database.ref(`characters/${actions[0].character.id}`).once('value');
            })
            .then((snapshot) => {
                expect(snapshot.val()).toEqual(characterData);
                done();
            });
    });

    it('should add character with defaults to Firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        const characterDefaults = {
            name: '',
            imageURL: '',
            levelId: '',
            characterClassId: '',
            experience: 0,
            strength: 0,
            dexterity: 0,
            constitution: 0,
            intelligence: 0,
            awareness: 0,
            luck: 0,
            gender: '',
            race: '',
            age: '',
            height: '',
            weight: '',
            description: '',
            background: '',
            personality: '',
            player: '',
            createdBy: '',
            archived: '',
            approvedBy: '',
            groupId: '',
        };

        // noinspection JSUnresolvedFunction
        store.dispatch(characterOperations.startAddCharacter())
            .then(() => {
                // noinspection JSUnresolvedFunction
                const actions = store.getActions();
                expect(actions[0]).toEqual({
                    type: types.ADD_CHARACTER,
                    character: {
                        id: expect.any(String),
                        ...characterDefaults,
                    }
                });

                return database.ref(`characters/${actions[0].character.id}`).once('value');
            })
            .then((snapshot) => {
                expect(snapshot.val()).toEqual(characterDefaults);
                done();
            });
    });

    it('should fetch characters from firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        // noinspection JSUnresolvedFunction
        store.dispatch(characterOperations.startSetCharacters()).then(() => {
            // noinspection JSUnresolvedFunction
            const action = store.getActions();
            expect(action[0]).toEqual({
                type: types.SET_CHARACTERS,
                characters
            });
            done();
        })
    });
});

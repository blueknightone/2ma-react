import types from './types';

/* State Shape
{
    id: string,
    name: string,
    imageURL: string,
    levelId: string,
    characterClassId: string,
    experience: number,
    strength: number,
    dexterity: number,
    constitution: number,
    intelligence: number,
    awareness: number,
    luck: number,
    gender: string,
    race: string,
    age: string,
    height: string,
    weight: string,
    description: string,
    background: string,
    personality: string,
    inventory: string[],
    player: string,
    createdBy: string,
    createdAt: number,
    archived: bool,
    approvedBy: string,
    groupId: string,
}
 */

const reducer = (state = [], action) => {
    switch (action.type) {
        case types.ADD_CHARACTER:
            return [
                ...state,
                action.character,
            ];
        case types.REMOVE_CHARACTER:
            return state.filter(({ id }) => id !== action.id);
        case types.EDIT_CHARACTER:
            return state.map(character => {
                if (character.id === action.id) {
                    character = {
                        ...character,
                        ...action.updates,
                    };

                }
                return character;
            });
        case types.SET_CHARACTERS:
            return action.characters;
        default:
            return state;
    }
};

export default reducer;

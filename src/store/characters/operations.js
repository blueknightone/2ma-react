import actions from './actions';
import database from '../../firebase/firebase';

const initialState = {
    name: '',
    imageURL: '',
    levelId: '',
    characterClassId: '',
    experience: 0,
    strength: 0,
    dexterity: 0,
    constitution: 0,
    intelligence: 0,
    awareness: 0,
    luck: 0,
    gender: '',
    race: '',
    age: '',
    height: '',
    weight: '',
    description: '',
    background: '',
    personality: '',
    player: '',
    createdBy: '',
    archived: '',
    approvedBy: '',
    groupId: '',
};

const startAddCharacter = (character = initialState) => (dispatch) => {
    return database.ref('characters').push(character)
        .then(ref => {
            dispatch(actions.addCharacter({
                id: ref.key,
                ...character,
            }))
        });
};

const startSetCharacters = () => (dispatch) => {
    return database.ref('characters').once('value')
        .then(snapshot => {
            const characters = [];
            snapshot.forEach(childSnapshot => {
                characters.push({
                    ...childSnapshot.val(),
                    id: childSnapshot.key,
                });
            });
            dispatch(actions.setCharacters(characters));
        });
};

const startUpdateCharacter = (id, updates) => dispatch => {
    return database.ref(`characters/${id}`).update(updates)
        .then(() => {
            dispatch(actions.editCharacter(id, updates));
        });
};

const startDeleteCharacter = ({ id } = {}) => dispatch => {
    return database.ref(`characters/${id}`).remove()
        .then(() => {
            dispatch(actions.removeCharacter({ id }));
        });
};

export default {
    startAddCharacter,
    startSetCharacters,
    startUpdateCharacter,
    startDeleteCharacter,
};

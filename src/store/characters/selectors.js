/**
 * Sorts without mutating original array
 * @param {[object]} characters
 * @returns {[object]} Sorted array
 */
const sortCharactersByName = (characters) => {
    const sortedCharacters = [...characters]; // copy array to keep from mutating original
    sortedCharacters.sort((a, b) => a.name > b.name ? 1 : -1);
    return sortedCharacters;
};

const findAllCharactersInGroup = (characters, groupId) => {
    return characters.filter(character => character.groupId === groupId);
};

export default {
    sortCharactersByName,
    findAllCharactersInGroup,
};

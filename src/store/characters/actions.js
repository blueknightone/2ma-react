import types from './types';

/**
 * Add a character to the redux store
 * @param {object} character
 * @returns {{character: object, type: string}}
 */
const addCharacter = character => ({
    type: types.ADD_CHARACTER,
    character
});

/**
 * Add all characters to the redux store
 * @param {Object[]} characters
 * @returns {{characters: Object[], type: string}}
 */
const setCharacters = characters => {
    return ({
        type: types.SET_CHARACTERS,
        characters
    });
};

/**
 * Update a character in the Redux store.
 * @param {string} id
 * @param {object} updates
 * @returns {{id: string, type: string, updates: object}}
 */
const editCharacter = (id, updates) => ({
    type: types.EDIT_CHARACTER,
    id,
    updates,
});

/**
 * Removes a character from the Redux store.
 * @param {string} id
 * @returns {{id: string, type: string}}
 */

const removeCharacter = ({id} = {}) => ({
    type: types.REMOVE_CHARACTER,
    id,
});

export default {
    addCharacter,
    setCharacters,
    editCharacter,
    removeCharacter,
};

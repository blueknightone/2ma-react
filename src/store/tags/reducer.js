import types from './types';

/* State Shape
{
    id: string,
    name: string,
    value: string,
}
 */

const reducer = (state = [], action) => {
    switch (action.type) {
        case types.ADD_TAG:
            return [
                ...state,
                action.tag,
            ];
        case types.REMOVE_TAG:
            return state.filter(({ id }) => id !== action.id);
        case types.EDIT_TAG:
            return state.map(tag => {
                if (tag.id === action.id) {
                    return {
                        ...tag,
                        ...action.updates,
                    };
                }

                return tag;
            });
        case types.SET_TAG:
            return action.tags;
        default:
            return state;
    }
};

export default reducer;

import actions from './actions';
import database from '../../firebase/firebase';

const initialState = {
    label: '',
    value: '',
};

const startAddTag = (tag = initialState) => dispatch => {
    return database.ref('tags').push(tag).then(ref => {
        dispatch(actions.addTag({
            id: ref.key,
            ...tag,
        }));
    });
};

const startSetTags = () => dispatch => {
    return database.ref('tags').once('value')
        .then(snapshot => {
            const tags = [];
            snapshot.forEach(childSnapshot => {
                tags.push({
                    ...childSnapshot.val(),
                    id: childSnapshot.key,
                });
            });
            dispatch(actions.setTags(tags));
        });
};

const startUpdateTag = (id, updates) => dispatch => {
    return database.ref(`tags/${id}`).update(updates)
        .then(() => {
            dispatch(actions.editTag(id, updates));
        });
};

const startRemoveTag = ({ id } = {}) => dispatch => {
    return database.ref(`tags/${id}`).remove()
        .then(() => {
            dispatch(actions.removeTag({ id }));
        });
};

export default {
    startAddTag,
    startSetTags,
    startUpdateTag,
    startRemoveTag,
};

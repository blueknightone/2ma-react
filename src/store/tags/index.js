import reducer from './reducer';

export { default as tagOperations } from './operations';

export default reducer;

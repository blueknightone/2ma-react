const ADD_TAG = 'tags/ADD_TAG';
const SET_TAG = 'tags/SET_TAG';
const EDIT_TAG = 'tags/EDIT_TAG';
const REMOVE_TAG = 'tags/REMOVE_TAG';

export default {
    ADD_TAG,
    SET_TAG,
    EDIT_TAG,
    REMOVE_TAG,
}

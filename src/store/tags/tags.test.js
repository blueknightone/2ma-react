import actions from './actions';
import types from './types';
import reducer from './reducer';
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import database from "../../firebase/firebase";
import { tagOperations } from "./index";

const tags = [
    { id: 'id-1', label: 'Tag One', value: 'tag-1' },
    { id: 'id-2', label: 'Tag Two', value: 'tag-2' },
];

describe('actions', function () {
    it('should setup addTag action object', function () {
        const action = actions.addTag(tags[0]);
        expect(action).toEqual({
            type: types.ADD_TAG,
            tag: tags[0]
        });
    });

    it('should setup setTags action object', () => {
        const action = actions.setTags(tags);
        expect(action).toEqual({ type: types.SET_TAG, tags });
    });

    it('should setup editTag action object', function () {
        const id = 'abc-123';
        const updates = { value: 'test' };
        const action = actions.editTag(id, updates);
        expect(action).toEqual({
            type: types.EDIT_TAG,
            id,
            updates,
        });
    });

    it('should setup removeTag action object', () => {
        const id = 'abc123';
        const action = actions.removeTag({ id });
        expect(action).toEqual({ type: types.REMOVE_TAG, id });
    });
});

describe('reducer', function () {
    it('should add tag to the store', function () {
        const action = actions.addTag(tags[0]);
        const state = reducer({ ...tags[0]}, action);
        expect(state).toEqual([{ ...tags[0] }]);
    });

    it('should remove tag from the store', () => {
        console.log('update test', tags);
        const action = actions.removeTag({ id: tags[0].id });
        const state = reducer(tags, action);
        expect(state).toEqual([tags[1]]);
    });
    it('should update a tag in the store', () => {
        const updatedTag = {
            ...tags[1],
            label: 'Updated Name',
        };
        console.log('update test', tags);
        const action = actions.editTag(tags[1].id, { label: 'Updated Name' });
        const state = reducer(tags, action);
        expect(state).toEqual([tags[0], updatedTag]);
    });
    it('should return all tags', function () {
        const action = actions.setTags(tags);
        const state = reducer({}, action);
        expect(state).toEqual(tags);
    });
    it('should return the state when no action matches', function () {
        const state = reducer('test-state', 'bad-action');
        expect(state).toEqual('test-state');
    });
});

describe('operations', function () {
    const createMockStore = configureMockStore([thunk]);
    const uid = 'this-is-my-test-uid';
    const defaultAuthState = { auth: { uid } };

    beforeEach((done) => {
        const tagData = {};
        tags.forEach((tag) => {
            tagData[tag.id] = { ...tag }
        });
        database.ref(`tags`).set(tagData).then(() => {
            done();
        });
    });

    it('should remove a tag from firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        const id = tags[1].id;
        // noinspection JSUnresolvedFunction
        store.dispatch(tagOperations.startRemoveTag({ id }))
            .then(() => {
                // noinspection JSUnresolvedFunction
                const actions = store.getActions({ auth: { uid } });
                expect(actions[0]).toEqual({
                    type: types.REMOVE_TAG,
                    id,
                });

                return database.ref(`characters/${id}`).once('value');
            })
            .then((snapshot) => {
                expect(snapshot.val()).toBeFalsy();
                done();
            });
    });

    it('should edit (update) a tag in Firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        const id = tags[1].id;
        const updates = { label: 'Test Name Change' };
        // noinspection JSUnresolvedFunction
        store.dispatch(tagOperations.startUpdateTag(id, updates))
            .then(() => {
                // noinspection JSUnresolvedFunction
                const actions = store.getActions();
                expect(actions[0]).toEqual({
                    type: types.EDIT_TAG,
                    id,
                    updates,
                });

                return database.ref(`tags/${id}`).once('value');
            })
            .then((snapshot) => {
                expect(snapshot.val().label).toEqual(updates.label);
                done();
            });
    });

    it('should add tag to Firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        const tagData = {
            label: 'Label Add Test',
            value: 'label-add-test',
        };
        // noinspection JSUnresolvedFunction
        store.dispatch(tagOperations.startAddTag(tagData))
            .then(() => {
                // noinspection JSUnresolvedFunction
                const actions = store.getActions();
                expect(actions[0]).toEqual({
                    type: types.ADD_TAG,
                    tag: {
                        id: expect.any(String),
                        ...tagData,
                    }
                });

                return database.ref(`tags/${actions[0].tag.id}`).once('value');
            })
            .then((snapshot) => {
                expect(snapshot.val()).toEqual(tagData);
                done();
            });
    });

    it('should fetch tags from firebase', (done) => {
        const store = createMockStore(defaultAuthState);
        // noinspection JSUnresolvedFunction
        store.dispatch(tagOperations.startSetTags())
            .then(() => {
            // noinspection JSUnresolvedFunction
            const action = store.getActions();
            expect(action[0]).toEqual({
                type: types.SET_TAG,
                tags
            });
            done();
        })
    });
});

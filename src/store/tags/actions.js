import types from './types';

const addTag = tag => ({
    type: types.ADD_TAG,
    tag,
});

const setTags = tags => ({
    type: types.SET_TAG,
    tags
});

const editTag = (id, updates) => ({
    type: types.EDIT_TAG,
    id,
    updates
});

const removeTag = ({ id } = {}) => ({
    type: types.REMOVE_TAG,
    id,
});


export default {
    addTag,
    setTags,
    editTag,
    removeTag,
}

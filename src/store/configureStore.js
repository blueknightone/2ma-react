import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunkMiddleware from 'redux-thunk';
import * as reducers from '.';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const rootReducer = combineReducers(reducers);

    return createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(
            thunkMiddleware,
        ))
    );
}

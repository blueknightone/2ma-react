import { firebase, googleAuthProvider } from "../../firebase/firebase";
import { login, logout } from "./actions";

const startGoogleLogin = () => () => firebase.auth().signInWithPopup(googleAuthProvider);

const startEmailSignUp = (email, password) => () => {
    firebase.auth().createUserWithEmailAndPassword(email, password)
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;

            return { errorCode, errorMessage };
        });
};

const startEmailSignIn = (email, password) => () => {
    firebase.auth().signInWithEmailAndPassword(email, password)
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;

            return { errorCode, errorMessage };
        });
};

const startLogout = () => () => firebase.auth().signOut();


export default {
    login,
    logout,
    startGoogleLogin,
    startEmailSignUp,
    startEmailSignIn,
    startLogout,
}

const LOGIN = 'session/LOGIN';
const LOGOUT = 'session/LOGOUT';

export default {
    LOGIN,
    LOGOUT,
};

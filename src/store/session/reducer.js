import types from './types';

/* State Shape
* {
*   uid: string,
* }
* */

const reducer = (state = {}, action) => {
    switch (action.type) {
        case types.LOGIN:
            return {
                uid: action.uid,
            };
        case types.LOGOUT:
            return {};
        default:
            return state;
    }
};

export default reducer;

import types from './types';

export const login = uid => ({
    type: types.LOGIN,
    uid,
});

export const logout = () => ({
    type: types.LOGOUT,
});

export default {
    login,
    logout,
};

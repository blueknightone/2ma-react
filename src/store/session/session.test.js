import actions from "./actions";
import reducer from './reducer';
import types from "./types";

describe('Session Actions', () => {
    test('should setup login action object', () => {
        const uid = '123abc';
        const action = actions.login(uid);
        expect(action).toEqual({ type: types.LOGIN, uid });
    });

    test('should setup logout action object', () => {
        const action = actions.logout();
        expect(action).toEqual({ type: types.LOGOUT });
    });
});

describe('Session Reducers', () => {
    test('should add uid to auth store', () => {
        const action = actions.login('123abd');
        const state = reducer({}, action);
        expect(state.uid).toEqual('123abd');
    });

    test('should return empty object on logout', () => {
        const action = actions.logout();
        const state = reducer({ uid: '123abc' }, action);
        expect(state).toEqual({})
    });
});


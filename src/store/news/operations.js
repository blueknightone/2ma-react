import actions from './actions';
import database from '../../firebase/firebase';

/**
 * Add news article to Firebase database and then dispatch to Redux store
 * @param {Object} newsData
 * @returns {function(function): Object}
 */
const startAddNews = (newsData = {}) => (dispatch) => {
    const {
        title = '',
        content = '',
        createdBy = '',
        createdAt = 0,
        tags = [],
        published = false,
    } = newsData;
    const article = { title, content, createdBy, createdAt, tags, published };

    return database.ref('news').push(article)
        .then(ref => {
            dispatch(actions.addNews({
                id: ref.key,
                ...article
            }));
        });
};

const startSetNews = () => (dispatch) => {
    return database.ref('news').once('value')
        .then(snapshot => {
            const news = [];
            snapshot.forEach(childSnapshot => {
                news.push({
                    id: childSnapshot.key,
                    ...childSnapshot.val(),
                });
            });

            dispatch(actions.setNews(news));
        });
};

const startRemoveNews = ({ id } = {}) => (dispatch) => {
    return database.ref(`news/${id}`).remove()
        .then(() => {
            dispatch(actions.removeNews({ id }))
        });
};

const startEditNews = (id, updates) => (dispatch) => {
    return database.ref(`news/${id}`).update(updates)
        .then(() => {
            dispatch(actions.editNews(id, updates))
        });
};

export default {
    startAddNews,
    startSetNews,
    startRemoveNews,
    startEditNews,
}

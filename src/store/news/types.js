const ADD_NEWS = '2-minute-attack/news/ADD_NEWS';
const SET_NEWS = '2-minute-attack/news/SET_NEWS';
const EDIT_NEWS = '2-minute-attack/news/EDIT_NEWS';
const REMOVE_NEWS = '2-minute-attack/news/REMOVE_NEWS';

export default {
    ADD_NEWS,
    SET_NEWS,
    EDIT_NEWS,
    REMOVE_NEWS,
};
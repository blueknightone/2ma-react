const sortNewsByDate = (news, sortOrder = 'des') => {
    if (news.length > 0) {
        switch (sortOrder) {
            case 'des':
                return news.sort((a, b) => a.createdAt < b.createdAt ? 1 : -1);
            case 'asc':
                return news.sort((a, b) => a.createdAt > b.createdAt ? 1 : -1);
            default:
                return news;
        }
    } else {
        return [];
    }
};

export default {
    sortNewsByDate,
}

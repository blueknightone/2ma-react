import reducer from './reducer';

export { default as newsSelectors } from './selectors';
export { default as newsOperations } from './operations';

export default reducer;

import types from './types';

/**
 * Add a new news article to the Redux store
 * @param {Object} article
 * @returns {{type: string, article: Object}}
 */
const addNews = article => ({
    type: types.ADD_NEWS,
    article,
});

/**
 * Adds all news articles to the redux store.
 * @param {Object[]} news
 * @returns {{news: Object[], type: string}}
 */
const setNews = news => {
    return ({
        type: types.SET_NEWS,
        news,
    });
};

/**
 * Updates a single news article in the Redux store
 * @param {string} id
 * @param {Object} updates
 * @returns {{id: string, type: string, updates: Object}}
 */
const editNews = (id, updates) => ({
    type: types.EDIT_NEWS,
    id,
    updates,
});

/**
 * Removes a news article from the Redux store.
 * @param {string} id
 * @returns {{id, type: string}}
 */
const removeNews = ({ id } = {}) => ({
    type: types.REMOVE_NEWS,
    id,
});

export default {
    addNews,
    setNews,
    editNews,
    removeNews,
}

import types from './types';

/* State Shape
{
    id: string,
    title: string,
    createdBy: string,
    createdAt: number,
    tags: string[],
    published: bool,
}
 */

const reducer = (state = [], action) => {
    switch (action.type) {
        case types.ADD_NEWS:
            return [
                ...state,
                action.article,
            ];
        case types.SET_NEWS:
            return action.news;
        case types.EDIT_NEWS:
            return state.map((article) => {
                if (article.id === action.id) {
                    return {
                        ...article,
                        ...action.updates,
                    }
                } else {
                    return article;
                }
            });
        case types.REMOVE_NEWS:
            return state.filter(({ id }) => {
                return id !== action.id;
            });
        default:
            return state;
    }
};

export default reducer;

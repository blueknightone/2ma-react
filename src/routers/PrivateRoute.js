import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from "react-router-dom";
import Header from "../views/components/Header/Header";
import NavPageHeader from "../views/components/NavPageHeader/NavPageHeader";

export const PrivateRoute = ({ isAuthenticated, component: Component, ...rest, }) => (
    <Route
        {...rest}
        component={(props) => (
            isAuthenticated ? (
                <React.Fragment>
                    <Header />
                    <NavPageHeader isAuthenticated={isAuthenticated} />
                    <main>
                        <Component {...props} />
                    </main>
                </React.Fragment>
            ) : (
                <Redirect to='/' />
            )
        )}
    />
);

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.session.uid,
});

export default connect(mapStateToProps)(PrivateRoute);

import React from 'react';
import { Redirect, Route } from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import Header from "../views/components/Header/Header";
import NavPageHeader from "../views/components/NavPageHeader/NavPageHeader";

export const PublicRoute = ({ isAuthenticated, component: Component, ...rest, }) => (
    <Route
        {...rest}
        component={(props) => {
            const path = props.match.path;
            const publicOnlyUrls = ['/login', '/sign-up', '/'];
            const isPublicOnly = publicOnlyUrls.includes(path); // Check if path is included in the publicOnlyUrls array
            return (
                // if page is for the public only and the user is authenticated, redirect to dashboard.
                isPublicOnly && isAuthenticated ? (
                    <Redirect to='/dashboard' />
                ) : (
                    <React.Fragment>
                        <Header />
                        <NavPageHeader isAuthenticated={isAuthenticated} />
                        <main className="content-container">
                            <Component {...props} />
                        </main>
                    </React.Fragment>
                )
            );
        }}
    />
);

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.session.uid,
});

export default connect(mapStateToProps)(PublicRoute);


import React from 'react';
import { Route, Router, Switch } from "react-router-dom";
import createHistory from 'history/createBrowserHistory';
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";
import DashboardPage from "../views/pages/DashboardPage/DashboardPage";
import HomePage from "../views/pages/HomePage/HomePage";
import LoginPage from "../views/pages/LoginPage/LoginPage";
import NotFoundPage from "../views/pages/NotFoundPage/NotFoundPage";
import StoriesPage from "../views/pages/Stories/StoriesPage";
import AddStoryPage from "../views/pages/Stories/AddStoryPage";
import EditStoryPage from "../views/pages/Stories/EditStoryPage";
import StoryPostsPage from "../views/pages/StoryPosts/StoryPostsPage";
import AddStoryPostPage from "../views/pages/StoryPosts/AddStoryPostPage";
import EditStoryPostPage from "../views/pages/StoryPosts/EditStoryPostPage";
import AddCharacterPage from "../views/pages/Characters/AddCharcterPage";
import WikiPage from "../views/pages/Wiki/WikiPage";
import AddWikiPage from "../views/pages/Wiki/AddWikiPage";
import EditWikiPage from "../views/pages/Wiki/EditWikiPage";
import PublicUsersPage from "../views/pages/Users/PublicUsersPage";
import EditProfilePage from "../views/pages/Users/EditProfilePage";
import AdminPage from "../views/pages/Admin/AdminPage";
import AdminUsersPage from "../views/pages/Admin/AdminUsersPage";
import AdminStoriesPage from "../views/pages/Admin/AdminStoriesPage";
import AdminStoryPostsPage from "../views/pages/Admin/AdminStoryPostsPage";
import AdminCharactersPage from "../views/pages/Admin/AdminCharactersPage";
import AdminWikiPage from "../views/pages/Admin/AdminWikiPage";
import SignUpPage from "../views/pages/SignUpPage";
import UserCharacterPage from "../views/pages/Users/UserCharactersPage";
import TagListPage from "../views/pages/TagListPage";
import NewsArticlePage from "../views/pages/NewsArticlePage/NewsArticlePage";
import AddNewsPage from "../views/pages/AddNewsPage/AddNewsPage";
import EditNewsPage from "../views/pages/EditNewsPage/EditNewsPage";
import Character from "../views/pages/Characters/CharacterDetails";
import EditCharacterPage from "../views/pages/Characters/EditCharacterPage";
import Characters from "../views/pages/Characters/Characters";

export const history = createHistory();

export const AppRouter = () => {
    return (
        <Router history={history}>
            <Switch>
                <PublicRoute path='/' exact={true} component={HomePage} />
                <PublicRoute path='/login' component={LoginPage} />
                <PublicRoute path='/sign-up' component={SignUpPage} />

                <PrivateRoute path='/dashboard' exact={true} component={DashboardPage} />

                <PublicRoute path='/tags/:tag?' component={TagListPage} />

                <PrivateRoute path='/news/editor/:id' component={EditNewsPage} />
                <PrivateRoute path='/news/editor' component={AddNewsPage} />
                <PublicRoute path='/news/:id' component={NewsArticlePage} />

                <PrivateRoute path='/stories/editor/:id' component={EditStoryPage} />
                <PrivateRoute path='/stories/editor' component={AddStoryPage} />
                <PrivateRoute path='/stories/:id/' component={StoryPostsPage} />
                <PrivateRoute path='/stories/:id/editor/:pid' component={EditStoryPostPage} />
                <PrivateRoute path='/stories/:id/editor' component={AddStoryPostPage} />
                <PublicRoute path='/stories' exact={true} component={StoriesPage} />

                <PrivateRoute path='/characters/editor/:id' component={EditCharacterPage} />
                <PrivateRoute path='/characters/editor' component={AddCharacterPage} />
                <PublicRoute path='/characters/:id' component={Character} />
                <PublicRoute path='/characters' exact={true} component={Characters} />

                <PublicRoute path='/wiki' exact={true} component={WikiPage} />
                <PrivateRoute path='/wiki/add' component={AddWikiPage} />
                <PrivateRoute path='/wiki/:id' component={EditWikiPage} />

                <PublicRoute path='/users' exact={true} component={PublicUsersPage} />
                <PublicRoute path='/users/:id' component={EditProfilePage} />
                <PublicRoute path='/users/:id/characters' component={UserCharacterPage} />

                <PrivateRoute path='/admin' exact={true} component={AdminPage} />
                <PrivateRoute path='/admin/users' component={AdminUsersPage} />
                <PrivateRoute path='/admin/stories' component={AdminStoriesPage} />
                <PrivateRoute path='/admin/posts' component={AdminStoryPostsPage} />
                <PrivateRoute path='/admin/characters' component={AdminCharactersPage} />
                <PrivateRoute path='/admin/wiki' component={AdminWikiPage} />

                <Route component={NotFoundPage} />
            </Switch>
        </Router>
    );
};

export default AppRouter;

import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter, { history } from "./routers/AppRouter";
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { firebase } from './firebase/firebase';
import { sessionOperations } from "./store/session";
import 'normalize.css/normalize.css';
import 'react-dates/lib/css/_datepicker.css';
import "react-sweet-progress/lib/style.css";
import './styles/styles.scss';
import LoadingPage from "./views/pages/LoadingPage/LoadingPage";

const store = configureStore();
const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);
let hasRendered = false;
const renderApp = () => {
    if (!hasRendered) {
        ReactDOM.render(jsx, document.getElementById('app'));
        hasRendered = true;
    }
};

ReactDOM.render(<LoadingPage />, document.getElementById('app'));

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        // Update Redux store with user information
        store.dispatch(sessionOperations.login(user.uid));
        renderApp();
        if (history.location.pathname === '/') {
            history.push('/dashboard');
        }
    } else {
        store.dispatch(sessionOperations.logout());
        renderApp();
        history.push('/');
    }
});

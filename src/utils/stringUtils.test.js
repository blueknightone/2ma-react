import { default as stringUtils } from './stringUtils';

describe('slugify', () => {
    it('should slugify a single word correctly', () => {
        const result = stringUtils.slugify('Test');
        expect(result).toEqual('test');
    });

    it('should slugify a sentence correctly', () => {
        const result = stringUtils.slugify('This Is a Test');
        expect(result).toEqual('this-is-a-test');
    });

    it('should handle special characters correctly', () => {
        const test = 'This has " & ^';
        const result = stringUtils.slugify(test);
        expect(result).toEqual('this-has-and');
    });

    it('should handle hyphens', () => {
        const test = 'This has - hyphens in the slug';
        const result = stringUtils.slugify(test);
        expect(result).toEqual('this-has-hyphens-in-the-slug');
    });

    it('should handle spaces at start and end of string', function () {
        const test = ' Space in front and behind ';
        const result = stringUtils.slugify(test);
        expect(result).toEqual('space-in-front-and-behind');
    });

    it('should replace special characters', function () {
        const test = 'special é character';
        const result = stringUtils.slugify(test);
        expect(result).toEqual('special-e-character');
    });
});

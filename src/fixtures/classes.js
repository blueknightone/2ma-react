const characterClasses = {};

characterClasses.starTrek = [
    {
        id: 'pos-commander-in-chief',
        name: 'Commander-in-Chief',
        description: "The commanding officer of the entire Starfleet. While they carry the rank of Fleet Admiral, they have authority over other fleet admirals.",
        imgURL: '/images/ranks/ds9-split/officer/red.png',
    }, {
        id: 'pos-fleet-admiral',
        name: 'Fleet Admiral',
        description: "The most senior flag rank in Starfleet. Admirals command a fleet of ships with the help of other admirals whether senior or junior. He can monitor, suspend, and assign captains to ships, send ships on particular missions, and decommission ships. As the situation warrants, he may take direct command of a ship.",
        imgURL: '/images/ranks/ds9-split/officer/red.png',
    }, {
        id: 'pos-admiral',
        name: 'Admiral',
        description: "Admirals command a fleet of ships with the help of other admirals whether senior or junior. He can monitor, suspend, and assign captains to ships, send ships on particular missions, and decommission ships. As the situation warrants, he may take direct command of a ship.",
        imgURL: '/images/ranks/ds9-split/officer/red.png',
    }, {
        id: 'pos-commanding-officer',
        name: 'Commanding Officer (CO)',
        description: "The officer in command of a unit, such as a ship's or starbase's crew. The commanding officer is responsible for the lives and actions of the personnel and equipment under their command. They are responsible for carrying out the orders of Starfleet and represent both Starfleet and the United Federation of Planets.",
        imgURL: '/images/ranks/ds9-split/officer/red.png',
    }, {
        id: 'pos-executive-officer',
        name: 'Executive Officer (XO)',
        description: "Acts as the liaison between the Commanding Officer and the crew. They act as disciplinarian, personnel manager, the captain's adviser, and more. They are also one of only two officers that can remove a Commanding Officer from duty. When the CO is not in active command, the XO takes over duties as CO.",
        imgURL: '/images/ranks/ds9-split/officer/red.png',
    }, {
        id: 'pos-second-officer',
        name: 'Second Officer',
        description: "When the Executive Officer is assigned duties away from his ship or base, they need the help of another officer to assume their role as XO. The second officer position is given to the highest ranked and trusted officer aboard. When required, the Second Officer will assume the role of XO or CO",
        imgURL: '/images/ranks/ds9-split/officer/red.png',
    }, {
        id: 'pos-mission-specialist',
        name: 'Mission Specialist',
        description: 'Advises the Commanding Officer and Executive Officer on mission-specific areas of importance. Typically the Mission Specialist knows as much or more as the CO and XO about a mission, and performs mission specific tasks.',
        imgURL: '/images/ranks/ds9-split/officer/red.png',
    }, {
        id: 'pos-chief-sec-tac',
        name: 'Chief Security/Tactical Officer',
        description: 'The Chief Security Officer, or Chief of Security, is responsible for the safety of ship or base, crew, and any visiting guests.',
        imgURL: '/images/ranks/ds9-split/officer/orange.png',
    }, {
        id: 'pos-chief-med',
        name: 'Chief Medical Officer',
        description: 'The chief medical officer (CMO), first medical officer or ship\'s surgeon was a senior staff-level position, typically held by a lieutenant commander or commander who was a doctor of medicine with surgical skills. A nurse could also hold this position if no qualified doctors were available.',
        imgURL: '/images/ranks/ds9-split/officer/teal.png',
    }
];

export default characterClasses;

import moment from "moment";

export const news = [
    {
        id: 'news-1',
        title: 'This is a longer news title. This is a longer news title. This is a longer news title.',
        content: "Lorem ipsum dolor sit amet, eu idque dolorum has. Id ubique verear volumus mea. In purto eligendi duo, posse debitis dolores vim et. Urbanitas temporibus pri in. Ad per adipiscing reformidans. Eu labore omnium latine sit, putent abhorreant pri ut, similique persequeris reformidans ius cu. Duis quaeque cum no.\n\nCongue semper voluptatum cu vel, no pri prompta minimum petentium, vel id solet graece. Ex vim tempor pertinax eleifend, ei elit nemore est. Luptatum recusabo dissentiunt vim an. Mei falli aliquam ornatus ei, no sed decore quaerendum. Ex eos mazim noster vivendum. Adhuc viderer singulis an eos, autem viderer et nam, vix te reque referrentur.\n\nConsul ceteros mei ex. Te pri quas iuvaret oportere, ex eam laudem singulis posidonium. Ei sea civibus detracto nominati. Ea eam dolor alterum, mel veritus civibus in, ad duo suas stet intellegat. Te duo consequat assueverit. Sea apeirian dissentiunt an, mea ei malis malorum. Cu pri nostro electram, mel cu dicit malorum molestie.\n\n",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(1, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-2',
        title: 'News Article 2',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(2, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: false,
    }, {
        id: 'news-3',
        title: 'News Article 3',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(3, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-4',
        title: 'News Article 4',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(4, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: false,
    }, {
        id: 'news-5',
        title: 'News Article 5',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(5, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-6',
        title: 'News Article 6',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(6, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: false,
    }, {
        id: 'news-7',
        title: 'News Article 7',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(7, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-8',
        title: 'News Article 8',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(8, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-9',
        title: 'News Article 9',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(9, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-10',
        title: 'News Article 10',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(10, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-11',
        title: 'News Article 11',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(11, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-12',
        title: 'News Article 12',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(12, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-13',
        title: 'News Article 13',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(13, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }, {
        id: 'news-14',
        title: 'News Article 14',
        content: "I know. I'm scared too.",
        createdBy: 'BlueKnightOne',
        createdAt: moment().subtract(14, 'days').valueOf(),
        tags: [{label:'tag-1', value:"tag-1"}, {label:'tag-2', value:"tag-2"}],
        published: true,
    }
];

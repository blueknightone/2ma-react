const characterGroups = [
    {
        id: 'group-1',
        name: 'U.S.S Enterprise',
    }, {
        id: "group-2",
        name: 'U.S.S. Titan',
    }
];

export default characterGroups;
const characterLevels = {};
characterLevels.starTrek = [
    {
        id: 'enl-cn',
        name: 'Crewman',
        shortName: 'CN',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e1.png',
    }, {
        id: 'enl-ca',
        name: 'Crewman Apprentice',
        shortName: 'CA',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e2.png',
    }, {
        id: 'enl-cr',
        name: 'Crewman Recruit',
        shortName: 'CR',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e1.png',
    }, {
        id: 'enl-po3',
        name: 'Petty Officer, 3rd Class',
        shortName: 'PO3',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e4.png',
    }, {
        id: 'enl-po2',
        name: 'Petty Officer, 2nd Class',
        shortName: 'PO2',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e5.png',
    }, {
        id: 'enl-po1',
        name: 'Petty Officer, 1st Class',
        shortName: 'PO1',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e6.png',
    }, {
        id: 'enl-cpo',
        name: 'Chief Petty Officer',
        shortName: 'CPO',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e7.png',
    }, {
        id: 'enl-scpo',
        name: 'Senior Chief Petty Officer',
        shortName: 'SCPO',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e8.png',
    }, {
        id: 'enl-mcpo',
        name: 'Master Chief Petty Officer',
        shortName: 'MCPO',
        imgURL: '/images/ranks/ds9-split/pips/navy_enlisted/e9.png',
    }, {
        id: 'off-cdt1',
        name: 'Cadet, 1st Year',
        shortName: 'CDT-1',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/c1.png',
    }, {
        id: 'off-cdt2',
        name: 'Cadet, 2nd Year',
        shortName: 'CDT-2',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/c2.png'
    }, {
        id: 'off-cdt3',
        name: 'Cadet, 3rd Year',
        shortName: 'CDT-3',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/c3.png'
    }, {
        id: 'off-cdt4',
        name: 'Cadet, 4th Year',
        shortName: 'CDT-4',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/c4.png'
    }, {
        id: 'off-ens',
        name: 'Ensign',
        shortName: 'EN',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/o1.png'
    }, {
        id: 'off-ltjg',
        name: 'Lieutenant, Junior Grade',
        shortName: 'LTJG',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/o2.png'
    }, {
        id: 'off-lt',
        name: 'Lieutenant',
        shortName: 'LT',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/o3.png'
    }, {
        id: 'off-ltcdr',
        name: 'Lieutenant Commander',
        shortName: 'LTCDR',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/o4.png'
    }, {
        id: 'off-cdr',
        name: 'Commander',
        shortName: 'CDR',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/o5.png'
    }, {
        id: 'off-capt',
        name: 'Captain',
        shortName: 'CAPT',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/o6.png'
    }, {
        id: 'off-fcapt',
        name: 'Fleet Captain',
        shortName: 'FCAPT',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/o6c.png'
    }, {
        id: 'off-rdml',
        name: 'Rear Admiral, Lower Half',
        shortName: 'RDML',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/a2.png'
    }, {
        id: 'off-radm',
        name: 'Rear Admiral, Upper Half',
        shortName: 'RADM',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/a2.png'
    }, {
        id: 'off-vadm',
        name: 'Vice Admiral',
        shortName: 'VADM',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/a3.png'
    }, {
        id: 'off-adm',
        name: 'Admiral',
        shortName: 'ADM',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/a4.png'
    }, {
        id: 'off-fadm',
        name: 'Fleet Admiral',
        shortName: 'FADM',
        imgURL: '/images/ranks/ds9-split/pips/navy_gold/a5.png'
    }
];

export default characterLevels;
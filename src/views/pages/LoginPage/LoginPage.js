import React from 'react';
import { connect } from 'react-redux'
import { sessionOperations } from "../../../store/session";

export const LoginPage = (props) => (
    <div className='box-layout'>
        <div className='box-layout__box'>
            <h1 className='box-layout__title'>2 Minute Attack</h1>
            <p>Select Login Method</p>
            <button className='button' onClick={props.startLogin}>Login with Google</button>
        </div>
    </div>
);

const mapDispatchToProps = (dispatch) => ({
    startLogin: () => dispatch(sessionOperations.startGoogleLogin())
});

export default connect(undefined, mapDispatchToProps)(LoginPage);

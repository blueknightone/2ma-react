import React from 'react';
import NewsList from "../../components/News/NewsList/NewsList";
import UpdateFeed from "../../components/UpdateFeed";

const HomePage = () => {
    return (
        <div id='home-page' className='content-container-group'>
            <div className="content-container">
                <NewsList />
            </div>
            <div className="content-container--narrow">
                <UpdateFeed />
            </div>
        </div>
    );
};

export default HomePage;

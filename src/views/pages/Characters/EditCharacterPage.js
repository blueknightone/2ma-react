import React, { Component } from 'react';
import CharacterEditor from '../../components/Characters/CharacterEditor';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { characterOperations } from "../../../store/characters";
import LoadingPage from "../LoadingPage/LoadingPage";

export class EditCharacterPage extends Component {

    componentDidMount() {
        this.props.startSetCharacters();
    }

    onSubmit = character => {
        this.props.startUpdateCharacter(this.props.character.id, character);
        this.props.history.push(`/characters/${this.props.character.id}`);
    };

    onRemove = () => {
        this.props.startDeleteCharacter({ id: this.props.character.id });
        this.props.history.push('/characters');
    };

    onCancel = () => {
        this.props.history.push(`/characters/${this.props.character.id}`);
    };

    render() {
        if (!this.props.character) return <LoadingPage />;
        return (
            <React.Fragment>
                <div className="content-container">
                    <h2>Update Character</h2>
                </div>
                <CharacterEditor
                    character={this.props.character}
                    onSubmit={this.onSubmit}
                    onRemove={this.onRemove}
                    onCancel={this.onCancel}
                />
            </React.Fragment>
        );
    }
}

EditCharacterPage.propTypes = {
    match: PropTypes.object,
    history: PropTypes.object,
};

const mapStateToProps = (state, props) => ({
    character: state.characters.find(character => character.id === props.match.params.id),
});

const mapDispatchToProps = {
    startSetCharacters: characterOperations.startSetCharacters,
    startUpdateCharacter: characterOperations.startUpdateCharacter,
    startDeleteCharacter: characterOperations.startDeleteCharacter,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditCharacterPage);

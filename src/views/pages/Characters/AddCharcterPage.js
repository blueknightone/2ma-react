import React, { Component } from 'react';
import CharacterEditor from "../../components/Characters/CharacterEditor";
import { characterOperations } from "../../../store/characters";
import { connect } from "react-redux";

class AddCharacterPage extends Component {
    onSubmit = character => {
        this.props.startAddCharacter(character);
        this.props.history.push(`/characters`);
    };

    onCancel = () => {
        this.props.history.push(`/characters`);
    };

    render() {
        return (
            <React.Fragment>
                <div className="content-container">
                    <h2>Create Character</h2>
                </div>
                <CharacterEditor
                    onSubmit={this.onSubmit}
                    onCancel={this.onCancel}
                />
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = {
    startAddCharacter: characterOperations.startAddCharacter,
};

export default connect(undefined, mapDispatchToProps)(AddCharacterPage);

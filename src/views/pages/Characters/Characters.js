import React, { Component } from 'react';
import characterGroups from "../../../fixtures/characterGroups";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import CharactersList from "../../components/Characters/CharactersList/CharactersList";
import { characterOperations } from "../../../store/characters";

class Characters extends Component {
    render() {
        let { characters, characterGroups, isAuthenticated } = this.props;
        let groupedCharacters = [];
        let ungroupedCharacters = [];

        characters.map(character => {
            if (character.groupId) {
                groupedCharacters.push(character);
            } else {
                ungroupedCharacters.push(character)
            }
        });

        return (
            <div id="character-list-page">
                {isAuthenticated && <Link to="/characters/editor" className="button pull-right">New Character</Link>}
                <h2>Characters</h2>
                {groupedCharacters.length > 0 && characterGroups.map(group => (
                    <CharactersList
                        key={group.id}
                        characters={groupedCharacters.filter(character => character.groupId === group.id)}
                        listName={group.name}
                    />
                ))}

                {ungroupedCharacters.length > 0 && (
                    <CharactersList
                        characters={characters.filter(character => !character.groupId)}
                        listName='Unassigned'
                    />
                )}

                {characters.length === 0 && (
                    <p className="empty-list-message">
                        There are no characters. <Link to="/characters/editor">Add some</Link>.
                    </p>
                )}
            </div>
        );
    }
}

Characters.propTypes = {
    characters: PropTypes.arrayOf(PropTypes.object).isRequired,
    characterGroups: PropTypes.any
};

const mapStateToProps = state => ({
    characters: state.characters,
    characterGroups: characterGroups,
    isAuthenticated: state.session.uid,
});

const mapDispatchToProps = dispatch => {
    dispatch(characterOperations.startSetCharacters());
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Characters);

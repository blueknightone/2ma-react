import React, { Component } from 'react';
import moment from "moment";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { characterOperations } from "../../../store/characters";
import CHARACTER_LEVELS from "../../../fixtures/levels";
// noinspection ES6CheckImport
import { Progress } from 'react-sweet-progress';
import CharacterPortrait from "../../components/Characters/CharacterPortrait";

// TODO: Complete items below.
/**
 * General component that renders the details of a character
 * @param {object} character
 * @param {string} uid
 */
class CharacterDetails extends Component {

    componentDidMount() {
        this.props.fetchCharacters();
    }

    render() {
        let { character, uid } = this.props;
        if (!character) return false;

        // TODO: Load from database instead of hard coded constant
        const characterLevel = CHARACTER_LEVELS['starTrek'].find(r => r.id === character.levelId);
        return (
            <div className="content-container">
                <div className="content-container content-container-group">
                    <div className="content-container--sidebar">
                        {/*Column 1*/}
                        <CharacterPortrait {...character} />
                    </div>
                    <div className="content-container">
                        {/*Column 2*/}

                        {/* TODO: Hide edit button if user does not own character or does not have permission */}
                        {
                            uid && (
                                <Link
                                    className="button pull-right"
                                    to={`/characters/editor/${character.id}`}>
                                    Edit Character
                                </Link>
                            )
                        }
                        <h1 className="character__name">{character.name}</h1>
                        <div className="character__description">
                            {character.description}
                            <h2>{characterLevel ? characterLevel.name : ''}</h2>
                            {characterLevel ? characterLevel.description : ''}
                        </div>

                        <div className="content-container-group">
                            <div className="content-container">
                                <div id="character-attributes" className="character-list">
                                    <h2 className="character-attributes--header">
                                        Attributes
                                    </h2>
                                    <div className="character-attributes-item">
                                        Strength: {character.strength}
                                    </div>
                                    <div className="character-attributes-item">
                                        Dexterity: {character.dexterity}
                                    </div>
                                    <div className="character-attributes-item">
                                        Constitution: {character.constitution}
                                    </div>
                                    <div className="character-attributes-item">
                                        Intelligence: {character.intelligence}
                                    </div>
                                    <div className="character-attributes-item">
                                        Awareness: {character.awareness}
                                    </div>
                                    <div className="character-attributes-item">
                                        Luck: {character.luck}
                                    </div>
                                </div>
                                {/* /#character-attributes */}
                            </div>

                            <div className="content-container">
                                <div id="character-bio" className="character-list">
                                    <h2 className="character-bio--header">
                                        Biographical Data
                                    </h2>
                                    <div className="character-bio-item">
                                        Gender: {character.gender}
                                    </div>
                                    <div className="character-bio-item">
                                        Race: {character.race}
                                    </div>
                                    <div className="character-bio-item">
                                        Age: {character.age}
                                    </div>
                                    <div className="character-bio-item">
                                        Height: {character.height}
                                    </div>
                                    <div className="character-bio-item">
                                        Weight: {character.weight}
                                    </div>
                                </div>
                                {/* /#character-physical-stats */}
                            </div>
                        </div>
                        <hr />
                        <div className="character-biography">
                            <h2>Background</h2>
                            <div className="character-biography__content">
                                {character.background ||
                                <p className="empty-list-message">No background information available.</p>}
                            </div>
                            <hr />
                            <h2>Personality</h2>
                            <div className="character-biography__content">
                                {character.personality ||
                                <p className="empty-list-message">No personality information available.</p>}
                            </div>
                        </div>
                        {/* /.character-biography */}

                        {character.inventory && (
                            <div id="character-inventory" className="character-list">
                                <h2 className="character-inventory--header">Inventory</h2>
                                {character.inventory.map((item, index) => (
                                    <div
                                        key={`${character.name}-inventory-${index}`}
                                        className="character-inventory-item">
                                        {item}
                                    </div>
                                ))}
                            </div>
                        )}
                    </div>
                </div>
                <div className="content-container">
                    {/*footer*/}
                    <hr />
                    <p>
                        Created by: {character.createdBy} on {moment(character.createdAt).format('MMMM DD, YYYY')}
                        {character.player ? (
                            character.player === uid ? (
                                <b> | Played by: {character.player} (You)</b>
                            ) : (
                                <span> | Played by {character.player}</span>
                            )
                        ) : (
                            <span> | Non-Player Character</span>
                        )}
                    </p>
                </div>
            </div>

        );
    }
}

const mapStateToProps = (state, props) => {
    return ({
        character: state.characters.find(character => character.id === props.match.params.id),
        uid: state.session.uid,
    });
};

const mapDispatchToProps = {
    fetchCharacters: characterOperations.startSetCharacters,
};

export default connect(mapStateToProps, mapDispatchToProps)(CharacterDetails);

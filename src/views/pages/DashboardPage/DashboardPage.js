import React from "react";
import { Link } from "react-router-dom";
import NewsList from "../../components/News/NewsList/NewsList";
import { connect } from "react-redux";

export const DashboardPage = ({ userId }) => (
    <div className="content-container content-container-group">
        <div className="content-container">
                <NewsList isCompact />
            <div className="story-list story-list--compact">
                <h2>Recent Story Posts</h2>
                <p>Recent posts here</p>
            </div>
        </div>
        <div className="content-container content-container--sidebar">
            <div className='actions-list'>
                <div className="actions-list__header">Actions</div>
                <div className='actions-list__item'>
                    <Link to='/news/editor'>Write News Post</Link>
                </div>
                <div className="actions-list__item">
                    <Link to='/stories/new'>Create New Story</Link>
                </div>
                <div className="actions-list__item">
                    <Link to={`/users/${userId}/characters`}>
                        Manage Characters
                    </Link>
                </div>
                <div className="actions-list__item">
                    <Link to={`/users/${userId}`}>
                        Manage Profile
                    </Link>
                </div>
            </div>
        </div>
    </div>
);

const mapStateToProps = (state) => ({
    userId: state.session.uid,
});

export default connect(mapStateToProps)(DashboardPage);

import React, { Component } from 'react';
import NewsEditorForm from "../../components/News/NewsEditorForm";
import { connect } from "react-redux";
import { newsOperations } from "../../../store/news";

export class EditNewsPage extends Component {
    onSubmit = article => {
        this.props.startEditNews(this.props.article.id, article);
        this.props.history.push('/');
    };

    onRemove = () => {
        this.props.startRemoveNews({ id: this.props.article.id });
        this.props.history.push('/');
    };

    onCancel = () => {
        this.props.history.push(`/news/${this.props.article.id}`);
    };

    render() {
        return (
            <div className="content-container">
                <h2>News Editor</h2>
                <NewsEditorForm
                    article={this.props.article}
                    onSubmit={this.onSubmit}
                    onRemove={this.onRemove}
                    onCancel={this.onCancel}
                />
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    article: state.news.find(article => article.id === props.match.params.id),
});

const mapDispatchToProps = {
    startEditNews: newsOperations.startEditNews,
    startRemoveNews: newsOperations.startRemoveNews,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditNewsPage);

import React from 'react';
import { shallow } from 'enzyme';
import { EditNewsPage } from "./EditNewsPage";
import { news } from "../../../fixtures/news";

let startEditNews, startRemoveNews, history, wrapper;

beforeEach(() => {
    startEditNews = jest.fn();
    startRemoveNews = jest.fn();
    history = { push: jest.fn() };
    wrapper = shallow(
        <EditNewsPage
            startEditNews={startEditNews}
            startRemoveNews={startRemoveNews}
            article={news[0]}
            history={history}
        />
    );
});

test('should render AddNewsPage correctly', () => {
    expect(wrapper).toMatchSnapshot();
});

test('should handle startEditNews', () => {
    wrapper.find('Connect(NewsEditorForm)').prop('onSubmit')(news[0]);
    expect(startEditNews).toHaveBeenCalledWith(news[0].id, news[0]);
    expect(history.push).toHaveBeenCalledWith('/');
});

test('should handle startRemoveNews', () => {
    const id = news[0].id;
    wrapper.find('Connect(NewsEditorForm)').prop('onRemove')(news[0]);
    expect(startRemoveNews).toHaveBeenCalledWith({ id });
    expect(history.push).toHaveBeenCalledWith('/');
});

test('should handle onCancel', ()=>{
    wrapper.find('Connect(NewsEditorForm)').prop('onCancel')(news[0]);
    expect(startRemoveNews).not.toHaveBeenCalled();
    expect(startEditNews).not.toHaveBeenCalled();
    expect(history.push).toHaveBeenCalledWith(`/news/${news[0].id}`);
});

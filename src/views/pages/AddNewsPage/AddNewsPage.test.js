import React from 'react';
import { shallow } from 'enzyme';
import { AddNewsPage } from "./AddNewsPage";
import { news } from "../../../fixtures/news";


let startAddNews, history, wrapper;

beforeEach(() => {
    startAddNews = jest.fn();
    history = { push: jest.fn() };
    wrapper = shallow(<AddNewsPage addNews={startAddNews} history={history} />)
});

test('should render AddNewsPage correctly', () => {
    expect(wrapper).toMatchSnapshot();
});

test('should handle onSubmit', () => {
    wrapper.find('Connect(NewsEditorForm)').prop('onSubmit')(news[1]);
    expect(history.push).toHaveBeenCalledWith('/');
    expect(startAddNews).toHaveBeenCalledWith(news[1]);
});

test('should handle onCancel', () => {
    wrapper.find('Connect(NewsEditorForm)').prop('onCancel')();
    expect(history.push).toHaveBeenCalledWith('/');
    expect(startAddNews).not.toHaveBeenCalled();
});

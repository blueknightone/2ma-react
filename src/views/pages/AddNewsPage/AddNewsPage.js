import React, { Component } from 'react';
import NewsEditorForm from "../../components/News/NewsEditorForm";
import { connect } from "react-redux";
import { newsOperations } from "../../../store/news";

export class AddNewsPage extends Component {
    onSubmit = (article) => {
        this.props.addNews(article);
        this.props.history.push('/');
    };

    onCancel = () => {
        this.props.history.push('/');
    };

    render() {
        return (
            <div className="content-container">
                <h2>News Editor - New Post</h2>
                <NewsEditorForm onSubmit={this.onSubmit} onCancel={this.onCancel} />
            </div>
        );
    }
}

const mapDispatchToProps = {
    addNews: newsOperations.startAddNews,
};

export default connect(undefined, mapDispatchToProps)(AddNewsPage);

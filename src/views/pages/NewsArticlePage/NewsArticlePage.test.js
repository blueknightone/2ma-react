import React from 'react';
import { shallow } from 'enzyme';
import { NewsArticlePage } from "./NewsArticlePage";
import { news } from "../../../fixtures/news";

describe('presentational component tests', () => {
    let wrapper;

    it('should render article without edit link when not logged in', () => {
        wrapper = shallow(
            <NewsArticlePage
                article={news[0]}
                uid={undefined}
                fetchNews={jest.fn()}
            />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('should render correctly with edit link when logged in', () => {
        wrapper = shallow(
            <NewsArticlePage
                article={news[0]}
                uid={'user-1234'}
                fetchNews={jest.fn()}
            />
        );
        expect(wrapper.find('Link').props().to).toBe(`/news/editor/${news[0].id}`);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render Loading Page if no article has returned', function () {
        wrapper = shallow(
            <NewsArticlePage
                uid={'user-1234'}
                fetchNews={jest.fn()}
            />
        );
        expect(wrapper.find('LoadingPage')).toHaveLength(1);
    });

    it('should not contain a TagList if article does not have tags', function () {
        const testArticle = {
            ...news[0],
            tags: undefined,
        };
        wrapper = shallow(
            <NewsArticlePage
                article={testArticle}
                uid={'user-1234'}
                fetchNews={jest.fn()}
            />
        );
        expect(wrapper.find('TagList')).toHaveLength(0);
    });
});

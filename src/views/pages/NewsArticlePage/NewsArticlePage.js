import React, { Component } from 'react';
import moment from "moment";
import NewsList from "../../components/News/NewsList/NewsList";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { newsOperations } from "../../../store/news";
import TagList from "../../components/TagList/TagList";
import LoadingPage from "../LoadingPage/LoadingPage";

export class NewsArticlePage extends Component {
    componentDidMount() {
        const { fetchNews } = this.props;
        fetchNews();
    }

    // TODO: Link to user profile (Ln 22)
    render() {
        let { article, uid } = this.props;
        if (!article) return <LoadingPage />;
        return (
            <div className="content-container-group">
                <div className="content-container">
                    <div className="news-article">
                        <div className="news-article__header">
                            {uid && <Link className="pull-right" to={`/news/editor/${article.id}`}>Edit</Link>}

                            <h2 className="news-article__title">{article.title}</h2>

                            <div className="news-article__author">

                                <img className="news-article__avatar" src="/images/favicon.png"
                                     alt="User Profile" />

                                <div className="news-article__by-line">
                                    <p className="bold">{article.createdBy}</p>
                                    <p className="italicize">{moment(article.createdAt).format('MMMM Do, YYYY [at] h:mm A')}</p>
                                </div>
                            </div>

                            {article.tags && <TagList tags={article.tags} />}
                        </div>

                        {article.content && (
                            <div className="news-article__body">
                                {article.content}
                            </div>
                        )}
                    </div>
                </div>

                <div className="content-container content-container--sidebar">
                    <NewsList isCompact />
                </div>
            </div>
        );
    }
}

NewsArticlePage.propTypes = {
    article: PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        content: PropTypes.string,
        createdAt: PropTypes.number,
        createdBy: PropTypes.string,
        published: PropTypes.bool,
        tags: PropTypes.array,
    }),
    uid: PropTypes.any
};

const mapStateToProps = (state, props) => ({
        article: state.news.find(article => article.id === props.match.params.id),
        uid: state.session.uid,
    });

const mapDispatchToProps = {
    fetchNews: newsOperations.startSetNews,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsArticlePage);

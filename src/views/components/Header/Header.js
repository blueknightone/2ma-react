import React from "react";
import { connect } from 'react-redux';
import { sessionOperations } from "../../../store/session";
import { Link } from "react-router-dom";

export const Header = (props) => (
    <header className='header'>
        <div className='content-container'>
            <div className='header__content'>
                <Link to='/' className='header__title'>
                    <h1>2 Minute Attack</h1>
                </Link>
                {props.isAuthenticated ? (
                    <button
                        className='button button--link'
                        onClick={props.startLogout}
                    >
                        Log Out
                    </button>
                ) : (
                    <div>
                        <Link to='/sign-up' className='button button--link'>
                            Sign Up
                        </Link>
                        <Link to='/login' className='button button--link'>
                            Log In
                        </Link>
                    </div>
                )}
            </div>
        </div>
    </header>
);

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.session.uid,
});

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(sessionOperations.startLogout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);

import React from 'react';
import PropTypes from 'prop-types';

// TODO: Set default foreground image through system settings
const RoleImage = ({fgImage, bgImage, center}) => {
    const rankClasses = ['split-rank'];
    if (center) rankClasses.push('split-rank--center');
    return (
        <div className={rankClasses.join(' ')}>
            <img
                className="split-rank__image"
                style={bgImage && {backgroundImage: `url(${bgImage})`}}
                src={fgImage}
                alt="Level Image"
            />
        </div>
    );
};

RoleImage.propTypes = {
    fgImage: PropTypes.string.isRequired,
    bgImage: PropTypes.string,
    center: PropTypes.bool,
};

// TODO: Create generic default image
RoleImage.defaultProps = {
    fgImage: '/images/ranks/ds9-split/officer/black.png',
};

export default RoleImage;

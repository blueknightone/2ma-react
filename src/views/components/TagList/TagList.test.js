import React from 'react';
import { shallow } from 'enzyme';
import { news } from "../../../fixtures/news";
import { TagList } from "./TagList";

describe('snapshot tests', function () {
    let wrapper;
    it('should render tag list correct with defaults', function () {
        wrapper = shallow(<TagList tags={news[0].tags} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render text only tag list', function () {
        wrapper = shallow(<TagList tags={news[0].tags} textOnly={true} />);
        expect(wrapper).toMatchSnapshot();
    });
});

import React from 'react';
import PropTypes from 'prop-types';

export const TagList = ({ tags, textOnly }) => {
    if (textOnly) {
        return (
            <p className="tag-list tag-list--compact">
                {tags.map((tag) => {
                    return <span className="tag-list__tag--text" key={tag.value}>{tag.label}</span>
                })}
            </p>
        )
    }
    return (
        <div className="tag-list">
            {tags ? tags.map((tag) => {
                return <div key={tag.value} className="tag-list__tag">{tag.label}</div>
            }) : undefined}
        </div>
    );
};

TagList.propTypes = {
    tags: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.string,
    })).isRequired,
    textOnly: PropTypes.bool,
};

export default TagList;

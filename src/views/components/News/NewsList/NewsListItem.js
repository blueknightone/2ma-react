import React from 'react';
import moment from "moment";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import TagList from "../../TagList/TagList";

const NewsListItem = (props) => {
    if (props.isCompact) {
        return (
            <React.Fragment>
                <Link className="news-item--compact" to={`/news/${props.id}`}>
                    <div className="news-item__title">
                        {`${props.title}${props.published ? '' : ' (DRAFT)'}`}
                    </div>
                    <div className="news-item__meta clearfix">
                        Author: {props.createdBy} - {moment(props.createdAt).format('MMMM D, YYYY [at] h:mm A')}
                        <div className="pull-right">{props.tags && <TagList tags={props.tags} textOnly />}</div>
                    </div>
                </Link>
            </React.Fragment>
        )
    }
    return (
        <div className="news-item">
            <div className="news-item__header">
                <h3 className="news-item__title">{props.title}</h3>
                <p className="news-item__meta">Written
                    by {props.createdBy} on {moment(props.createdAt).format('MMMM Do, YYYY [at] h:mm A')}</p>
            </div>
            <div className="news-item__body">
                {props.content.length > 200 ? (
                    <div className="news-item__body">
                        {props.content.substr(0, 200)}...
                        <p><Link to={`/news/${props.id}`}>Read more...</Link></p>
                    </div>
                ) : (
                    <div className="news-item__body">
                        {props.content}
                    </div>
                )}
            </div>
            <div className="news-item__footer clearfix">
                {props.tags && <TagList tags={props.tags} />}
            </div>
        </div>
    );
};

NewsListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    createdBy: PropTypes.string.isRequired,
    createdAt: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
    tags: PropTypes.array,
};

export default NewsListItem;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NewsListItem from "./NewsListItem";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ToggleCheckbox from "../../Inputs/ToggleCheckbox";
import { newsOperations, newsSelectors } from "../../../../store/news";
import LoadingPage from "../../../pages/LoadingPage/LoadingPage";

class NewsList extends Component {
    state = {
        showDraft: false,
    };

    toggleDraft = () => {
        this.setState(prevState => ({ showDraft: !prevState.showDraft }));
    };

    render() {
        if (!this.props.news) return <LoadingPage />;
        return (
            <div className="content-container">
                <div className="news-list__header">
                    <h2>Recent News &amp; Announcements</h2>
                </div>
                < div className="news-list">
                    {this.props.uid && (
                        <ToggleCheckbox
                            name="showDraft"
                            checked={this.state.showDraft}
                            onChange={this.toggleDraft}
                        />
                    )}
                    {this.props.news.length === 0 ? (
                        <p className="empty-list-message">
                            There is nothing to show here. <Link to="/news/editor">Add something</Link>.
                        </p>
                    ) : (
                        this.props.news.map((article) => {
                            if (this.state.showDraft || article.published) {
                                return (
                                    <NewsListItem
                                        key={article.id}
                                        {...article}
                                        isCompact={this.props.isCompact}
                                    />
                                );
                            }
                        })
                    )}
                </div>
            </div>
        );
    }
}

NewsList.propTypes = {
    /** Show basic information or article previews */
    isCompact: PropTypes.bool,
};

NewsList.defaultProps = {
    isCompact: false,
};

const mapStateToProps = state => {
    return ({
        news: newsSelectors.sortNewsByDate(state.news, 'des'),
        uid: state.session.uid,
    });
};

const mapDispatchToProps = dispatch => {
    dispatch(newsOperations.startSetNews());
    return {};
};


export default connect(mapStateToProps, mapDispatchToProps)(NewsList);

import React, { Component } from 'react';
import moment from "moment";
import 'react-dates/initialize';
import { SingleDatePicker } from 'react-dates';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import ToggleCheckbox from "../Inputs/ToggleCheckbox";
import LabeledInput from "../Inputs/LabeledInput";
import CreatableSelect from 'react-select/lib/Creatable';
import { stringUtils } from "../../../utils";
import { tagOperations } from "../../../store/tags";

class NewsEditorForm extends Component {
    state = {
        title: this.props.article ? this.props.article.title : '',
        content: this.props.article ? this.props.article.content : '',
        createdBy: this.props.article ? this.props.article.createdBy : this.props.uid,
        createdAt: this.props.article ? moment(this.props.article.createdAt) : moment(),
        tags: this.props.article ? this.props.article.tags : [],
        published: this.props.article ? this.props.article.published : true,
        calendarFocused: false,
        error: '',
    };

    //<editor-fold desc="Member Methods">
    handleInputChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value,
        });
    };

    handleTags = (articleTags) => {
        const tags = [];
        articleTags.map(tag => {
            tag.__isNew__ && this.props.startAddTag({ label: tag.label, value: tag.value });
            const label = tag.label.trim();
            const value = stringUtils.slugify(tag.value);
            tags.push({ label, value });
        });
        this.setState({ tags });
    };

    onDateChange = (createdAt) => {
        if (createdAt) {
            this.setState({ createdAt });
        }
    };

    onFocusChange = ({ focused }) => {
        this.setState({ calendarFocused: focused });
    };

    onSubmit = (e) => {
        e.preventDefault();
        const article = {
            title: this.state.title,
            content: this.state.content,
            createdBy: this.state.createdBy,
            createdAt: this.state.createdAt.valueOf(),
            tags: this.state.tags,
            published: this.state.published,
        };

        // Validate state
        // TODO: UX - provide validation feedback against all fields at the same time

        if (!article.title) {
            this.setState({ error: "Please provide a title." });
        } else if (!article.content) {
            this.setState({ error: "Please provide some content." });
        } else if (!article.createdBy) {
            this.setState({ error: "Please select an author." });
        } else if (!article.createdAt) {
            this.setState({ error: "Please select a publication date." })
        } else {
            this.setState({ error: '' });
        }

        // If no validation errors, pass state up to parent.
        if (this.state.error === '') {
            this.props.onSubmit({ ...article });
        }
    };

    onRemove = (e) => {
        e.preventDefault();
        this.props.onRemove();
    };

    //</editor-fold>

    render() {
        // TODO: Complete items below
        return (
            <form id="news-editor" onSubmit={this.onSubmit}>
                {this.state.error && <p className="form__error">{this.state.error}</p>}
                <div className="content-container-group">
                    <div className="content-container">
                        <LabeledInput
                            onChange={this.handleInputChange}
                            value={this.state.title}
                            name="title"
                            placeholder="Enter a title here..."
                            header
                            srOnly
                        />

                        <div className="input-group input-group--inline">
                            <label htmlFor="tags">Tags</label>
                            <CreatableSelect
                                className="input-group__control"
                                onChange={this.handleTags}
                                defaultValue={this.state.tags}
                                options={this.props.tags}
                                isMulti
                            />
                        </div>

                        <LabeledInput
                            name="content"
                            type="textarea"
                            value={this.state.content}
                            onChange={this.handleInputChange}
                            placeholder="Write your content here..."
                            srOnly
                        />

                    </div>

                    {/* TODO: Refactor into searchable dropdown list of users */}
                    <div className="content-container content-container--sidebar">
                        <div className="input-group">
                            <label htmlFor="createdBy">Author</label>
                            {/* TODO: Disable if user does not have correct permissions */}
                            <select
                                name="createdBy"
                                value={this.state.createdBy}
                                onChange={this.handleInputChange}
                                className="input-group__control"
                            >
                                {/* TODO: Get usernames from database */}
                                <option value="">Select an Author</option>
                                <option value="OAA866fkjsgwhfOdbMRfzHaRfzG2">BlueKnightOne</option>
                            </select>
                        </div>

                        <div className="input-group">
                            <label htmlFor="createdAt">Publish Date</label>
                            <SingleDatePicker
                                id="createdAt"
                                date={this.state.createdAt}
                                onDateChange={this.onDateChange}
                                focused={this.state.calendarFocused}
                                onFocusChange={this.onFocusChange}
                                numberOfMonths={1}
                                isOutsideRange={() => false}
                                block
                            />
                        </div>

                        <div className="input-group input-group--inline">
                            <ToggleCheckbox
                                name="published"
                                checked={this.state.published}
                                onChange={this.handleInputChange}
                            />
                        </div>

                        <div className="button-group button-group--block">
                            <input
                                type="submit"
                                value="Save"
                                className="button button--primary"
                            />
                            <button
                                type="button"
                                className="button button--secondary"
                                onClick={this.props.onCancel}
                            >
                                Cancel
                            </button>
                            {this.props.onRemove && (
                                <button
                                    className="button button--secondary"
                                    onClick={this.props.onRemove}
                                >
                                    Delete
                                </button>
                            )}
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

NewsEditorForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onRemove: PropTypes.func,
};

const mapStateToProps = (state) => ({
    uid: state.session.uid,
    tags: state.tags
});

const mapDispatchToProps = {
    startAddTag: tagOperations.startAddTag,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsEditorForm);

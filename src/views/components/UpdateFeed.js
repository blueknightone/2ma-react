import React from 'react';

const UpdateFeed = () => {
    return (
        <div>
            <h2>Update List</h2>
            <div>
                <div>
                    <span>Some User</span> did something somewhere <span>some time ago</span>.
                </div>
                <div>
                    <span>Some User</span> did something somewhere <span>some time ago</span>.
                </div>
                <div>
                    <span>Some User</span> did something somewhere <span>some time ago</span>.
                </div>
                <div>
                    <span>Some User</span> did something somewhere <span>some time ago</span>.
                </div>
                <div>
                    <span>Some User</span> did something somewhere <span>some time ago</span>.
                </div>
            </div>
        </div>
    );
};

export default UpdateFeed;

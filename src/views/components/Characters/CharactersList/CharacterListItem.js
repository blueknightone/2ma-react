import React from 'react';
import PropTypes from 'prop-types';
import RoleImage from "../../RoleImages/RoleImage";
import { Link } from "react-router-dom";
import characterClasses from "../../../../fixtures/classes";
import characterLevels from "../../../../fixtures/levels";

const CharacterListItem = ({ character: { id, name, characterClassId, levelId, player } }) => {
    const characterClass = characterClasses['starTrek'].find(c => c.id === characterClassId);
    const characterLevel = characterLevels['starTrek'].find(r => r.id === levelId);
    return (
        <Link className="character-list__item" to={`/characters/${id}`}>
            <div className="character-list-item-rank">
                <RoleImage
                    bgImage={characterClass ? characterClass.imgURL : undefined}
                    fgImage={characterLevel ? characterLevel.imgURL : undefined}
                    center
                />
            </div>

            <div className="character-list-item-info">
                <div className="character-info__name">
                    {name}
                </div>
                <div className="character-info__role">
                    {characterClass ? characterClass.name : 'Unassigned'}
                </div>
                <div className="character-info__player-name">
                    {player ? `Played by: ${player}` : "Non-Player Character"}
                </div>
            </div>
        </Link>
    );
};

CharacterListItem.propTypes = {
    character: PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        player: PropTypes.string.isRequired,
        levelId: PropTypes.string.isRequired,
        characterClassId: PropTypes.string.isRequired,
    }).isRequired,
};

export default CharacterListItem;

import React from 'react';
import CharacterListItem from "./CharacterListItem";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';

// TODO: Add pagination
const CharactersList = ({ characters, listName }) => (
    <div className="character-list">
        <h3 className="character-list__header">{listName}</h3>
        {characters && characters.length === 0 ? (
            <p className="empty-list-message">
                There are no characters in this group. <Link to="/characters/editor">Add something</Link>.
            </p>
        ) : characters.map((character) => <CharacterListItem key={character.id} character={character} />)}
    </div>
);

CharactersList.propTypes = {
    characters: PropTypes.arrayOf(PropTypes.object).isRequired,
    listName: PropTypes.string.isRequired,
};

export default CharactersList;

import React from 'react';
import PropTypes from 'prop-types';
import { Line } from "rc-progress";
import RoleImage from "../RoleImages/RoleImage";
import CHARACTER_CLASSES from "../../../fixtures/classes";
import CHARACTER_LEVELS from "../../../fixtures/levels";

const CharacterPortrait = ({ name, imageURL, characterClassId, levelId, experience }) => {
    let imageRole;
    const characterClass = CHARACTER_CLASSES['starTrek'].find(c => c.id === characterClassId);
    const characterLevel = CHARACTER_LEVELS['starTrek'].find(r => r.id === levelId);
    if (characterClass.imgURL && characterLevel.imgURL) {
        imageRole =
            <RoleImage bgImage={characterClass.imgURL} fgImage={characterLevel.imgURL} center />
    } else if (characterClass.imgURL || characterLevel.imgURL) {
        imageRole = <RoleImage fgImage={characterClass.imgURL || characterLevel.imgURL} center />;
    } else {
        imageRole = <p className="character-role__label">
            {characterLevel.name}
        </p>
    }
    return (
        <div className="character-portrait">
            {imageURL && <img
                className="character-portrait__image"
                src={imageURL}
                alt={`Character portrait for ${name}`}
            />}
            <div className="character-role">
                {/* TODO: Add single image and text only versions */}
                {imageRole}
                <p className="character-role__label">
                    {characterClass.name}
                </p>
            </div>
            {/* TODO: Wire up experience points */}
            {/* TODO: Implement level XP thresholds */}
            <div className="character-experience">
                <p className="character-experience__label">XP: {experience.toLocaleString()} /
                    50,000</p>
                <Line
                    className="character-experience__bar"
                    percent={(experience / 50000.00 * 100).toFixed(2)}
                    strokeWidth={5}
                    trailWidth={5}
                />
            </div>
        </div>
    );
};

CharacterPortrait.propTypes = {
    name: PropTypes.string.isRequired,
    imageURL: PropTypes.string.isRequired,
};

export default CharacterPortrait;

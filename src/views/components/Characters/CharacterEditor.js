import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { characterPortraitStorage } from "../../../firebase/firebase";
import moment from "moment";

import LabeledInput from "../Inputs/LabeledInput";
import NumericInput from "../Inputs/NumericInput";
import ToggleCheckbox from "../Inputs/ToggleCheckbox";
import RoleImage from "../RoleImages/RoleImage";
import { Line } from "rc-progress";

import characterClasses from "../../../fixtures/classes";
import characterLevels from "../../../fixtures/levels";

// TODO: Get characterGroups from database
// TODO: Remove this array
const playerArray = [
    { value: 'player-1', label: 'Player 1' },
    { value: 'player-2', label: 'Player 2' },
    { value: 'OAA866fkjsgwhfOdbMRfzHaRfzG2', label: 'BlueKnightOne' },
];

class CharacterEditor extends Component {
    // Set initial state
    state = {
        character: {
            id: this.props.character ? this.props.character.id : '',
            name: this.props.character ? this.props.character.name : '',
            imageURL: this.props.character ? this.props.character.imageURL : '',
            levelId: this.props.character ? this.props.character.levelId : '',
            characterClassId: this.props.character ? this.props.character.characterClassId : '',
            strength: this.props.character ? this.props.character.strength : 5,
            dexterity: this.props.character ? this.props.character.dexterity : 5,
            constitution: this.props.character ? this.props.character.constitution : 5,
            intelligence: this.props.character ? this.props.character.intelligence : 5,
            awareness: this.props.character ? this.props.character.awareness : 5,
            luck: this.props.character ? this.props.character.luck : 5,
            gender: this.props.character ? this.props.character.gender : '',
            race: this.props.character ? this.props.character.race : '',
            age: this.props.character ? this.props.character.age : '',
            height: this.props.character ? this.props.character.height : '',
            weight: this.props.character ? this.props.character.weight : '',
            skinTone: this.props.character ? this.props.character.skinTone : '',
            hairColor: this.props.character ? this.props.character.hairColor : '',
            description: this.props.character ? this.props.character.description : '',
            background: this.props.character ? this.props.character.background : '',
            personality: this.props.character ? this.props.character.personality : '',
            inventory: this.props.character ? this.props.character.inventory : [],
            player: this.props.character ? this.props.character.player : '',
            experience: this.props.character ? this.props.character.experience : 0,
            createdBy: this.props.character ? this.props.character.createdBy : '',
            createdAt: this.props.character ? moment(this.props.character.createdAt) : moment(),
            approved: this.props.character ? this.props.character.approved : '',
            archived: this.props.character ? this.props.character.archived : false,
            groupId: this.props.character ? this.props.character.groupId : '',
        },
        errors: {},
        selectedFile: '',
        progress: '',
        isUploading: '',
    };

    handleInputChange = (e) => {
        // Get the data from the form
        const target = e.target;
        const name = target.name;
        let value = target.type === 'checkbox' ? target.checked : target.value;

        // Update state
        this.setState(prevState => ({
            character: {
                ...prevState.character,
                [name]: value
            },
        }));
    };

    handleToggleApproved = () => {
        this.setState(({
            character: {
                ...this.state.character,
                approved: this.state.character.approved ? '' : this.props.uid,
            }
        }))
    };

    onClickRandomizeAttributes = (e) => {
        e.preventDefault();
        const attrs = ['strength', 'dexterity', 'constitution', 'intelligence', 'awareness', 'luck'];
        const min = Math.ceil(0);
        const max = Math.floor(10);
        return attrs.forEach((attr) => {
            const roll = Math.floor(Math.random() * (max - min + 1) + min);
            let e = {
                target: {
                    name: attr,
                    value: roll,
                }
            };
            this.handleInputChange(e);
        });
    };

    handleFileUpload = (e) => {
        e.preventDefault();
        const file = e.target.files[0]; // Get the file from the input
        const fileNameArray = file.name.split('.'); // split the filename apart so we can get the extension
        const uploadTask = characterPortraitStorage //In the character portrait storage
            .child(`character${this.state.character.id}.${fileNameArray[fileNameArray.length - 1]}`) // standardize the file name on the character ID
            .put(file); // Upload the file

        this.setState({ progress: 0, isUploading: true, }); // Setup the progress bar

        uploadTask.on('state_changed', snapshot => { // Listen to the state_changed event on the upload
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100; // calculate the percent of the file uploaded
            this.setState({ progress }); // update the progress bar
        }, error => {
            this.setState({ errors: { ...this.state.errors, imageURL: error.code } });
        }, () => {
            uploadTask.snapshot.ref.getDownloadURL().then(imageURL => {
                this.setState({
                    character: {
                        ...this.state.character,
                        imageURL
                    },
                    isUploading: false,
                })
            })
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();

        // reset errors
        this.setState({
            errors: {}
        });

        // Character name is really the only required
        if (this.state.character.name === '') {
            return this.setState({
                errors: {
                    ...this.state.errors,
                    name: 'Character Name cannot be blank.'
                }
            });
        }

        const createdAt = this.state.character.createdAt.valueOf();
        const inventory = this.state.character.inventory || null;

        return this.props.onSubmit({
            ...this.state.character,
            createdAt,
            inventory,
        });
    };

    handleCancel = (e) => {
        e.preventDefault();
        this.props.onCancel();
    };

    handleDelete = (e) => {
        e.preventDefault();
        this.props.onRemove();
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="content-container content-container-group">
                    <div className="content-container">
                        <LabeledInput
                            onChange={this.handleInputChange}
                            value={this.state.character.name}
                            name="name"
                            errorMessage={this.state.errors.name}
                            header={true}
                            placeholder="Character Name"
                        />
                        {this.props.character ? (
                            <div className="content-container-group">
                                <div className="content-container">
                                    {this.state.character && this.state.character.imageURL ? (
                                        <img
                                            style={{ maxWidth: '200px', marginRight: '2rem' }}
                                            src={this.state.character.imageURL}
                                            alt="Uploaded Character Portrait"
                                        />
                                    ) : (
                                        /* TODO: Create placeholder image */
                                        <img
                                            style={{ maxWidth: '200px', marginRight: '2rem' }}
                                            src="http://via.placeholder.com/200x300?text=Placeholder"
                                            alt="Placeholder character image"
                                        />
                                    )}
                                </div>
                                <div className="content-container">
                                    <LabeledInput
                                        type="file"
                                        onChange={this.handleFileUpload}
                                        value={''}
                                        name="imageURL"
                                        label="Select an image"
                                    />
                                    <Line percent={this.state.progress} />
                                </div>
                            </div>
                        ) : <p>You will be able to upload an image after creating a character.</p>}
                        <div className="content-container-group">
                            <div className="content-container">
                                <NumericInput
                                    value={this.state.character.strength}
                                    name="strength"
                                    onChange={this.handleInputChange}
                                    min={0}
                                    max={10}
                                    inline
                                />

                                <NumericInput
                                    value={this.state.character.dexterity}
                                    name="dexterity"
                                    onChange={this.handleInputChange}
                                    min={0}
                                    max={10}
                                    inline
                                />

                                <NumericInput
                                    value={this.state.character.constitution}
                                    name="constitution"
                                    onChange={this.handleInputChange}
                                    min={0}
                                    max={10}
                                    inline
                                />

                                <NumericInput
                                    value={this.state.character.intelligence}
                                    name="intelligence"
                                    onChange={this.handleInputChange}
                                    min={0}
                                    max={10}
                                    inline
                                />

                                <NumericInput
                                    value={this.state.character.awareness}
                                    name="awareness"
                                    onChange={this.handleInputChange}
                                    min={0}
                                    max={10}
                                    inline
                                />

                                <NumericInput
                                    value={this.state.character.luck}
                                    name="luck"
                                    onChange={this.handleInputChange}
                                    min={0}
                                    max={10}
                                    inline
                                />

                                <div className="button-group--block">
                                    <button
                                        className="button"
                                        onClick={this.onClickRandomizeAttributes}
                                    >
                                        Randomize All
                                    </button>
                                </div>
                            </div>

                            <div className="content-container">
                                <LabeledInput
                                    onChange={this.handleInputChange}
                                    value={this.state.character.gender}
                                    name="gender"
                                    inline
                                />

                                <LabeledInput
                                    onChange={this.handleInputChange}
                                    value={this.state.character.race}
                                    name="race"
                                    inline
                                />

                                <LabeledInput
                                    onChange={this.handleInputChange}
                                    value={this.state.character.age}
                                    name="age"
                                    inline
                                />

                                <LabeledInput
                                    onChange={this.handleInputChange}
                                    value={this.state.character.height}
                                    name="height"
                                    inline
                                />

                                <LabeledInput
                                    onChange={this.handleInputChange}
                                    value={this.state.character.weight}
                                    name="weight"
                                    inline
                                />

                                <LabeledInput
                                    onChange={this.handleInputChange}
                                    value={this.state.character.skinTone}
                                    name="skinTone"
                                    inline
                                />

                                <LabeledInput
                                    onChange={this.handleInputChange}
                                    value={this.state.character.hairColor}
                                    name="hairColor"
                                    inline
                                />
                            </div>
                        </div>

                    </div>
                    <div className="content-container content-container--sidebar">
                        <div className="input-group">
                            <label htmlFor="characterClassId">Class/Role</label>
                            <select
                                name="characterClassId"
                                value={this.state.character.characterClassId}
                                onChange={this.handleInputChange}
                                className="input-group__control"
                            >
                                <option value="">Choose a Class</option>
                                {characterClasses.starTrek.map((c) => {
                                    return <option key={c.id} value={c.id}>{c.name}</option>;
                                })}
                            </select>
                        </div>

                        <div className="input-group">
                            <label htmlFor="levelId">Level/Rank</label>
                            <select
                                name="levelId"
                                value={this.state.character.levelId}
                                onChange={this.handleInputChange}
                                className="input-group__control"
                            >
                                <option value="">Choose a Class</option>
                                {characterLevels.starTrek.map((l) => {
                                    return <option key={l.id} value={l.id}>{l.name}</option>;
                                })}
                            </select>
                        </div>

                        {this.state.character.characterClassId && this.state.character.levelId && (
                            <div className="input-group">
                                <RoleImage
                                    fgImage={characterLevels.starTrek.find(l => this.state.character.levelId === l.id).imgURL}
                                    bgImage={characterClasses.starTrek.find(c => this.state.character.characterClassId === c.id).imgURL}
                                    center
                                />
                                <p className="help-block">{characterClasses.starTrek.find(c => c.id === this.state.character.characterClassId).description}</p>
                            </div>
                        )}

                        <div className="input-group">
                            <label htmlFor="player">Player</label>
                            <select
                                name="player"
                                value={this.state.character.value}
                                onChange={this.handleInputChange}
                                className="input-group__control"
                            >
                                <option value="">Non-Player-Character</option>
                                {playerArray.map((player) => {
                                    return <option key={player.value} value={player.value}>{player.label}</option>;
                                })}
                            </select>
                        </div>

                        <div className="input-group input-group--inline">
                            <ToggleCheckbox
                                name="approved"
                                checked={!!this.state.character.approved}
                                onChange={this.handleToggleApproved}
                            />
                        </div>

                        <div className="input-group input-group--inline">
                            <ToggleCheckbox
                                name="archived"
                                checked={this.state.character.archived}
                                onChange={this.handleInputChange}
                            />
                        </div>

                        <div className="button-group button-group--block">
                            <button
                                className="button"
                                onClick={this.handleSubmit}
                            >
                                Save Character
                            </button>
                            <button
                                className="button button--secondary"
                                onClick={this.handleCancel}
                            >
                                Cancel
                            </button>
                            {this.props.character && <button
                                className="button button--secondary"
                                onClick={this.handleDelete}
                            >
                                Delete Character
                            </button>}
                        </div>
                    </div>
                </div>

                <div className="content-container">
                    <LabeledInput
                        name="description"
                        value={this.state.character.description}
                        onChange={this.handleInputChange}
                        type="textarea"
                    />

                    <LabeledInput
                        name="background"
                        value={this.state.character.background}
                        onChange={this.handleInputChange}
                        type="textarea"
                    />

                    <LabeledInput
                        name="personality"
                        value={this.state.character.personality}
                        onChange={this.handleInputChange}
                        type="textarea"
                    />
                </div>
            </form>
        );
    }
}

CharacterEditor.propTypes = {
    character: PropTypes.object,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onRemove: PropTypes.func,
};

const mapStateToProps = state => ({
    uid: state.session.uid,
    characters: state.characters,
});

export default connect(mapStateToProps)(CharacterEditor);

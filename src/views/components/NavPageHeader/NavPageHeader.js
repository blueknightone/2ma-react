import { NavLink } from "react-router-dom";
import React from "react";

const NavPageHeader = ({ isAuthenticated }) => (
    <div className="page-header page-header--nav">
        <div className="content-container">
            <div className="page-header__actions">
                {isAuthenticated ? (
                    <NavLink className="button button--secondary" activeClassName="active" to="/dashboard" exact>
                        Dashboard
                    </NavLink>
                ) : (
                    <NavLink className="button button--secondary" activeClassName="active" to="/" exact>
                        Home
                    </NavLink>
                )}
                <NavLink className="button button--secondary" activeClassName="active" to="/stories">
                    Stories
                </NavLink>
                <NavLink className="button button--secondary" activeClassName="active" to="/characters">
                    Characters
                </NavLink>
                <NavLink className="button button--secondary" activeClassName="active" to="/users">
                    Users
                </NavLink>
                <NavLink className="button button--secondary" activeClassName="active" to="/wiki">
                    Wiki
                </NavLink>
                {isAuthenticated && (
                    <NavLink className="button button--secondary" activeClassName="active" to="/admin">
                        Administration
                    </NavLink>
                )}
            </div>
        </div>
    </div>
);

export default NavPageHeader;

import React from 'react';
import { shallow } from 'enzyme';
import NavPageHeader from "./NavPageHeader";

test('should render correctly when not logged in', () => {
    const wrapper = shallow(<NavPageHeader isAuthenticated={false} />);
    expect(wrapper).toMatchSnapshot();
});

test('should render correctly when not logged in', () => {
    const wrapper = shallow(<NavPageHeader isAuthenticated={true} />);
    expect(wrapper).toMatchSnapshot();
});

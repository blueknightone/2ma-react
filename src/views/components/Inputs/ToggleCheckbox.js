import React from 'react';
import PropTypes from 'prop-types'

const ToggleCheckbox = props =>(
    <label htmlFor={props.name}>
        <div className='humanize'>{props.name.replace(/([A-Z])/g, " $1")}</div>
        <input
            id={props.name}
            className="toggle-check-input"
            type="checkbox"
            name={props.name}
            checked={props.checked}
            onChange={props.onChange}
        />
        <span className="toggle-check-text" />
    </label>
);

ToggleCheckbox.propTypes = {
    name: PropTypes.string.isRequired,
    checked: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default ToggleCheckbox;
import React from 'react';
import PropTypes from 'prop-types';

const LabeledInput = ({
                          name,
                          value,
                          onChange,
                          type = 'text',
                          label,
                          placeholder,
                          inline,
                          header,
                          errorMessage,
                          srOnly
                      }) => {
    let groupStyles = ['input-group'];
    if (inline) groupStyles.push('input-group--inline');
    if (errorMessage) groupStyles.push('input-group--error');

    let inputStyles = ['input-group__control'];
    if (header) inputStyles.push('text-input--header');

    const renderLabel = (
        header || srOnly ? (
            <label htmlFor={name} className="sr-only">{label || name.replace(/([A-Z])/g, " $1")}</label>
        ) : (
            <label htmlFor={name} className="humanize">{label || name.replace(/([A-Z])/g, " $1")}</label>
        )
    );

    const renderInput = () => {
        let elInput;
        switch (type) {
            case 'textarea':
                inputStyles.push('textarea--block');
                elInput = (
                    <textarea
                        name={name}
                        className={inputStyles.join(' ')}
                        onChange={onChange}
                        value={value}
                        placeholder={placeholder}
                    />
                );
                break;
            default:
                elInput = (
                    <input
                        type={type}
                        name={name}
                        className={inputStyles.join(' ')}
                        onChange={onChange}
                        value={value}
                        placeholder={placeholder}
                    />
                );
        }
        return elInput
    };

    return (
        <div className={groupStyles.join(' ')}>
            {renderLabel}
            {renderInput()}
            {errorMessage && <p className="help-block humanize">{errorMessage}</p>}
        </div>
    );
};

LabeledInput.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    errorMessage: PropTypes.string,
    inline: PropTypes.bool,
    header: PropTypes.bool,
    srOnly: PropTypes.bool,
    label: PropTypes.string,
};

export default LabeledInput;

import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NumericInput extends Component {
    state = {
        value: this.props.value,
    };

    componentWillReceiveProps({ value }) {
        this.setState({ value });
    }

    handleIncrement = (e) => {
        e.preventDefault();
        const name = this.props.name;
        let value;

        if (this.props.max && this.state.value >= this.props.max) {
            value = this.state.value;
        } else {
            value = this.state.value + 1
        }

        this.setState({ value });
        this.props.onChange({ target: { name, value } });
    };

    handleDecrement = (e) => {
        e.preventDefault();
        const name = this.props.name;
        let value;

        if (this.props.max && this.state.value <= this.props.min) {
            value = this.state.value;
        } else {
            value = this.state.value - 1
        }

        this.setState({ value });
        this.props.onChange({ target: { name, value } });
    };

    render() {
        let groupStyles = ['input-group'];
        if (this.props.inline) groupStyles.push('input-group--inline');
        return (
            <div className={groupStyles.join(' ')}>
                <label htmlFor={this.props.name} className="humanize">
                    {this.props.name.replace(/([A-Z])/g, " $1")}
                </label>
                <div className="input-group--add-on-group">
                    <button
                        className="button input-group__add-on"
                        onClick={this.handleIncrement}
                    >
                        {'\u002B'}
                    </button>
                    <input
                        type="text"
                        className="input-group__control input-group__control--numeric"
                        value={this.state.value}
                        name={this.props.name}
                        min={this.props.min}
                        onChange={() => {}}
                    />
                    <button
                        className="button input-group__add-on"
                        onClick={this.handleDecrement}
                    >
                        {'\u002d'}
                    </button>
                </div>
            </div>
        );
    }
}

NumericInput.propTypes = {
    value: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    min: PropTypes.number,
    max: PropTypes.number,
    inline: PropTypes.bool,
};

export default NumericInput;

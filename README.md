# 2 Minute Attack

A web-based application for play-by-post role-playing games.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Setup a [Firebase](https://firebase.google.com/) project to host your development data. **Do not use your production database for development!**

### Installing - Development

1. Clone or download the project.
2. Create a `.env.development` file and copy the contents of .env.sampleconfig and replace the values with the project specific configuration from Firebase.
    ```dotenv
    FIREBASE_API_KEY=SomeApiKey
    
    FIREBASE_AUTH_DOMAIN=some-project-name.firebaseapp.com
    
    FIREBASE_DATABASE_URL=https://some-project-name.firebaseio.com
    
    FIREBASE_PROJECT_ID=some-project-name
    
    FIREBASE_STORAGE_BUCKET=some-projec-name.appspot.com
    
    FIREBASE_MESSAGING_SENDER_ID=1234567890
    ```
3. Run `yarn dev-server` from the termnal.
4. Open a browser to `http://localhost:8080`

## Running the tests

It's recommended to create a separate Firebase project for testing. `2-minute-attack` uses `jest` and `enzyme` for testing.

1. Create a `.env.test` file and copy the contents of .env.sampleconfig and replace the values with the project specific configuration from Firebase.
     ```dotenv
     FIREBASE_API_KEY=SomeApiKey
     
     FIREBASE_AUTH_DOMAIN=some-project-name.firebaseapp.com
     
     FIREBASE_DATABASE_URL=https://some-project-name.firebaseio.com
     
     FIREBASE_PROJECT_ID=some-project-name
     
     FIREBASE_STORAGE_BUCKET=some-projec-name.appspot.com
     
     FIREBASE_MESSAGING_SENDER_ID=1234567890
     ```
2. Run `yarn test` to run all tests once or `yarn test-watch` to run all tests in watch mode.

Component tests use `.toMatchSnapshot()` from the `jest` testing framework. Changing components will require you to update the snapshot in order for the test to pass

## Deployment

For easy deployment to Heroku, click the button below:

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

### Manual Deployment

1. Setup a [Firebase](https://firebase.google.com/) project to host your production data.
2. Setup the following environment variables:
     ```dotenv
     NODE_ENV=production   
  
     FIREBASE_API_KEY=SomeApiKey
     
     FIREBASE_AUTH_DOMAIN=some-project-name.firebaseapp.com
     
     FIREBASE_DATABASE_URL=https://some-project-name.firebaseio.com
     
     FIREBASE_PROJECT_ID=some-project-name
     
     FIREBASE_STORAGE_BUCKET=some-projec-name.appspot.com
     
     FIREBASE_MESSAGING_SENDER_ID=1234567890
     ```
3. From the terminal run `yarn build:prod && yarn start`. The application will start by default on `http://localhost:3000`

## Built With

* [Firebase](https://firebase.google.com/) - Dependency Management
* [Webpack](https://webpack.js.org/) - Dependency Management
* [Facebook React](https://reactjs.org/)
* [Node JS](https://nodejs.org/en/) - Used to generate RSS Feeds

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Justin Abbott** - *Initial work* - [BlueKnightOne](https://github.com/BlueKnightOne)

## Acknowledgments

Special thanks to [Anodyne Productions](http://anodyne-productions.com) for the fantastic [Nova](https://github.com/anodyne/nova) project. 


# Customizing

**Warning: Advanced Topic**

The file and folder structure is below. Areas to pay attention to for customization are marke

```
/src
    /firebase - Firebase configration
    /routers
        AppRouter.js - Application Routes
        PrivateRoute.js - Defines a route requiring authentication
        PublicRoute.js - Defines a route that doesn't require authentication 
    /store
        configureStore.js - Combines all the reducers and configures the Redux store
        /ducks
            index.js - Interface for Redux stores
            /<module
                actions.js - Redux actions
                index.js - Module store interface
                operations.js - Firebase API calls
                reducers.js - Redux reducers
                selectors.js - Redux selectors
                types.js - Action type constants
                test.js - Redux store tests
    **/styles
        styles.scss - Imports all styles
        /base
            *_base.scss - Global styles
            *_settings - Configuragle variables
        /components
            _<component-name>.scss - Styles for individual components**
```

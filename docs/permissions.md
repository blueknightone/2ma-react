# Roles

These are the assignable roles for each module. See the module section below for what permissions the default roles grant.

Custom roles can be created and assigned permissions through Admin > Roles 

## News
| Role            | Create | Read | Update | Delete |
|:----------------|:------:|:----:|:------:|:------:|
| `none`          | N      | Y    | N      | N      |
| `user`          | N      | Y    | N      | N      |
| `news-editor`   | Y      | Y    | Owned  | Owned  |
| `administrator` | Y      | Y    | All    | All    |

## Characters
| Role                  | Create | Read | Update | Delete |
|:----------------------|:------:|:----:|:------:|:------:|
| `none`                | N      | Y    | N      | N      |
| `user`                | Y*     | Y    | Owned  | Owned  |
| `characters-editor`   | Y      | Y    | All    | All    |
| `administrator`       | Y      | Y    | All    | All    |
\* Users can be limited to the number of characters they are allowed to create

## Stories
| Role            | Create | Read | Update | Delete |
|:----------------|:------:|:----:|:------:|:------:|
| `none`          | N      | Y    | N      | N      |
| `user`          | N      | Y    | N      | N      |
| `story-editor`  | Y      | Y    | Owned  | Owned  |
| `administrator` | Y      | Y    | All    | All    |

## Story Posts
| Role            | Create | Read | Update        | Delete       |
|:----------------|:------:|:----:|:-------------:|:------------:|
| `none`          | N      | Y    | N             | N            |
| `user`          | Y      | Y    | Owned         | Owned        |
| `story-editor`  | Y      | Y    | Story Owned * | Story Owned* |
| `administrator` | Y      | Y    | All           | All          |
\* Story editors can edit any post within a story they created.

## Wiki
| Role            | Create | Read | Update | Delete |
|:----------------|:------:|:----:|:------:|:------:|
| `none`          | N      | Y    | N      | N      |
| `user`          | Y      | Y    | Y      | N      |
| `wiki-editor`   | Y      | Y    | Y      | Y      |
| `administrator` | Y      | Y    | Y      | Y      |
